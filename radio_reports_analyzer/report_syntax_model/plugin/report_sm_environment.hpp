// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_ENVIRONMENT_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_ENVIRONMENT_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample
{
	namespace FileSystem
	{
		class Accessor;
	}
}

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Plugin {

/*---------------------------------------------------------------------------*/

class Environment
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	virtual ~Environment() {}

/*---------------------------------------------------------------------------*/

	virtual boost::shared_ptr< FileSystem::Accessor > getFileSystem() = 0;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Plugin
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_ENVIRONMENT_HPP__
