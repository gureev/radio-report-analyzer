// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#include "radio_reports_analyzer/report_syntax_model/sources/ph/report_sm_ph.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/accessor/report_sm_accessor_impl.hpp"

#include "radio_reports_analyzer/report_syntax_model/plugin/report_sm_plugin_instance.hpp"

#include "framework/filesystem/plugin/fs_plugin_identifier.hpp"
#include "framework/filesystem/api/fs_accessor.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Plugin {

/*---------------------------------------------------------------------------*/

CONNECTOR_DECLARE_PLUGIN_FACTORY()

/*---------------------------------------------------------------------------*/

Instance::Instance()
	:	m_accessor()
{
} // Instance::Instance

/*---------------------------------------------------------------------------*/

Instance::~Instance()
{
} // Instance::~Instance

/*---------------------------------------------------------------------------*/

void
Instance::onPluginLoad()
{
	m_accessor.reset(
		new Implementation::Accessor(
			* getFileSystem()
		)
	);

} // Instance::onPluginLoad

/*---------------------------------------------------------------------------*/

boost::shared_ptr< FileSystem::Accessor >
Instance::getFileSystem()
{
	return getPluginAccessor< FileSystem::Accessor >( FileSystem::Plugin::Identifier );

} // Instance::getFileSystem

/*---------------------------------------------------------------------------*/

boost::shared_ptr< Connector::Accessor >
Instance::getAccessor() const
{
	return m_accessor;

} // Instance::getAccessor

/*---------------------------------------------------------------------------*/

const Connector::PluginIdentifier &
Instance::getIdentifier() const
{
	return Identifier;

} // Instance::getIdentifier

/*---------------------------------------------------------------------------*/

} // namespace Plugin
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample
