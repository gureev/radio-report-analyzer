// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_PLUGIN_IDENTIFIER_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_PLUGIN_IDENTIFIER_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/connector/api/cn_plugin_identifier.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Plugin {

/*---------------------------------------------------------------------------*/

// {95AF7421-1DAA-4388-88DF-EED52F4E4C41}
const Connector::PluginIdentifier Identifier
( 0x95af7421, 0x1daa, 0x4388, 0x88, 0xdf, 0xee, 0xd5, 0x2f, 0x4e, 0x4c, 0x41 );


/*---------------------------------------------------------------------------*/

} // namespace Plugin
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_PLUGIN_IDENTIFIER_HPP__
