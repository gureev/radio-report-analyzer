// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_PLUGIN_INSTANCE_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_PLUGIN_INSTANCE_HPP__

/*---------------------------------------------------------------------------*/

#include "radio_reports_analyzer/report_syntax_model/api/report_sm_accessor.hpp"

#include "radio_reports_analyzer/report_syntax_model/plugin/report_sm_plugin_identifier.hpp"
#include "radio_reports_analyzer/report_syntax_model/plugin/report_sm_environment.hpp"

#include "framework/connector/api/cn_plugin.hpp"
#include "framework/connector/api/cn_accessor.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Plugin {

/*---------------------------------------------------------------------------*/

class Instance
	:	public Connector::Plugin
	,	public Environment
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	Instance();

	/*virtual*/ ~Instance();

/*---------------------------------------------------------------------------*/

	/*virtual*/ boost::shared_ptr< Connector::Accessor > getAccessor() const;

	/*virtual*/ const Connector::PluginIdentifier & getIdentifier() const;

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	/*virtual*/ boost::shared_ptr< FileSystem::Accessor > getFileSystem();

/*---------------------------------------------------------------------------*/

	/*virtual*/ void onPluginLoad();

/*---------------------------------------------------------------------------*/

	boost::shared_ptr< Accessor > m_accessor;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Plugin
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_PLUGIN_INSTANCE_HPP__
