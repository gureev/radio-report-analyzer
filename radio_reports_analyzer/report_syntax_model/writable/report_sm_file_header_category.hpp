// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HEADER_CATEGORY_WRITABLE_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HEADER_CATEGORY_WRITABLE_HPP__

/*---------------------------------------------------------------------------*/

#include "radio_reports_analyzer/report_syntax_model/api/report_sm_file_header_category.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Writable {

/*---------------------------------------------------------------------------*/

class FileHeaderCategory
	:	public ReportSyntaxModel::FileHeaderCategory
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	virtual void setAssisted( CategoryAssisted::Enum _assisted ) = 0;

	virtual void setBand( CategoryBand::Enum _band ) = 0;

	virtual void setMode( CategoryMode::Enum _mode ) = 0;

	virtual void setOperator( CategoryOperator::Enum _operator ) = 0;

	virtual void setPower( CategoryPower::Enum _power ) = 0;

	virtual void setStation( CategoryStation::Enum _station ) = 0;

	virtual void setTime( CategoryTime::Enum _time ) = 0;

	virtual void setTransmitter( CategoryTransmitter::Enum _transmitter ) = 0;

	virtual void setOverlay( CategoryOverlay::Enum _overlay ) = 0;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Writable
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HEADER_CATEGORY_WRITABLE_HPP__
