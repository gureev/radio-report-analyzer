// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_WRITABLE_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_WRITABLE_HPP__

/*---------------------------------------------------------------------------*/

#include "radio_reports_analyzer/report_syntax_model/api/report_sm_file.hpp"
#include "radio_reports_analyzer/report_syntax_model/api/report_sm_line_code_kind.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample
{
	namespace RadioReportsAnalyzer
	{
		namespace ReportSyntaxModel
		{
			class Time;
		}
	}
}

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Writable {

/*---------------------------------------------------------------------------*/

class Line;
class FileHeader;

/*---------------------------------------------------------------------------*/

class File
	:	public ReportSyntaxModel::File
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	virtual void setHeader( std::auto_ptr< Writable::FileHeader > _fileHeader ) = 0;

/*---------------------------------------------------------------------------*/

	virtual Writable::Line & pushLine(
			LineCodeKind::Enum _codeKind
		,	int _frequency
		,	const std::string & _mode
		,	std::auto_ptr< Time > _time
	) = 0;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Writable
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_WRITABLE_HPP__
