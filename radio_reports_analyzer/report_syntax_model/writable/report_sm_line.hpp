// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_LINE_WRITABLE_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_LINE_WRITABLE_HPP__

/*---------------------------------------------------------------------------*/

#include "radio_reports_analyzer/report_syntax_model/api/report_sm_line.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Writable {

/*---------------------------------------------------------------------------*/

class Line
	:	public ReportSyntaxModel::Line
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	virtual void setSent( std::auto_ptr< Operator > _operator ) = 0;

	virtual void setReceived( std::auto_ptr< Operator > _operator ) = 0;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Writable
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_LINE_WRITABLE_HPP__
