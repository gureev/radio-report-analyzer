// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HEADER_WRITABLE_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HEADER_WRITABLE_HPP__

/*---------------------------------------------------------------------------*/

#include "radio_reports_analyzer/report_syntax_model/api/report_sm_file_header.hpp"
#include "radio_reports_analyzer/report_syntax_model/api/report_sm_category_assisted.hpp"
#include "radio_reports_analyzer/report_syntax_model/api/report_sm_category_band.hpp"
#include "radio_reports_analyzer/report_syntax_model/api/report_sm_category_mode.hpp"
#include "radio_reports_analyzer/report_syntax_model/api/report_sm_category_operator.hpp"
#include "radio_reports_analyzer/report_syntax_model/api/report_sm_category_power.hpp"
#include "radio_reports_analyzer/report_syntax_model/api/report_sm_category_station.hpp"
#include "radio_reports_analyzer/report_syntax_model/api/report_sm_category_time.hpp"
#include "radio_reports_analyzer/report_syntax_model/api/report_sm_category_transmitter.hpp"
#include "radio_reports_analyzer/report_syntax_model/api/report_sm_category_overlay.hpp"
#include "radio_reports_analyzer/report_syntax_model/api/report_sm_category_contest.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Writable {

/*---------------------------------------------------------------------------*/

class FileHeader
	:	public ReportSyntaxModel::FileHeader
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	// START-OF-LOG:
	virtual void setLogVersion( const CommonHeaders::Version & _version ) = 0;

	// CALLSIGN:
	virtual void setCallsign( const std::string & _callsign ) = 0;

	// CLAIMED-SCORE:
	virtual void setClaimedScore( int _claimedScore ) = 0;

	// CREATED-BY:
	virtual void setCreatedBy( const std::string & _createdBy ) = 0;

	// EMAIL:
	virtual void setEmail( const std::string & _email ) = 0;

	// LOCATION:
	virtual void setLocation( const std::string & _location ) = 0;

	// NAME:
	virtual void setName( const std::string & _name ) = 0;

/*---------------------------------------------------------------------------*/

	// ADDRESS:
	virtual void pushAddress( const std::string & _address ) = 0;

	// ADDRESS-CITY:
	virtual void setAddressCity( const std::string & _city ) = 0;

	// ADDRESS-STATE-PROVINCE:
	virtual void setAddressState( const std::string & _state ) = 0;

	// ADDRESS-POSTALCODE:
	virtual void setAddressPostalCode( const std::string & _postalCode ) = 0;

	// ADDRESS-COUNTRY:
	virtual void setAddressCountry( const std::string & _country ) = 0;

/*---------------------------------------------------------------------------*/

	// CATEGORY-ASSISTED:
	virtual void setCategoryAssisted( CategoryAssisted::Enum _assisted ) = 0;

	// CATEGORY-BAND:
	virtual void setCategoryBand( CategoryBand::Enum _band ) = 0;

	// CATEGORY-MODE:
	virtual void setCategoryMode( CategoryMode::Enum _mode ) = 0;

	// CATEGORY-OPERATOR:
	virtual void setCategoryOperator( CategoryOperator::Enum _operator ) = 0;

	// CATEGORY-POWER:
	virtual void setCategoryPower( CategoryPower::Enum _power ) = 0;

	// CATEGORY-STATION:
	virtual void setCategoryStation( CategoryStation::Enum _station ) = 0;

	// CATEGORY-TIME:
	virtual void setCategoryTime( CategoryTime::Enum _time ) = 0;

	// CATEGORY-TRANSMITTER:
	virtual void setCategoryTransmitter( CategoryTransmitter::Enum _transmitter ) = 0;

	// CATEGORY-OVERLAY:
	virtual void setCategoryOverlay( CategoryOverlay::Enum _overlay ) = 0;

/*---------------------------------------------------------------------------*/

	// CLUB:
	virtual void pushClub( const std::string & _club ) = 0;

/*---------------------------------------------------------------------------*/

	// CONTEST:
	virtual void setContest(
			const std::string & _contestName
		,	CategoryContest::Enum _contest
	) = 0;

/*---------------------------------------------------------------------------*/

	// OFFTIME:
	virtual void setOfftime(
			std::auto_ptr< Time > _beginTime
		,	std::auto_ptr< Time > _endTime
	) = 0;

/*---------------------------------------------------------------------------*/

	// OPERATORS:
	virtual void pushOperator( const std::string & _operator ) = 0;

/*---------------------------------------------------------------------------*/

	// SOAPBOX:
	virtual void pushSoapboxLine( const std::string & _line ) = 0;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Writable
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HEADER_WRITABLE_HPP__
