// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HEADER_CONTEST_WRITABLE_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HEADER_CONTEST_WRITABLE_HPP__

/*---------------------------------------------------------------------------*/

#include "radio_reports_analyzer/report_syntax_model/api/report_sm_file_header_contest.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Writable {

/*---------------------------------------------------------------------------*/

class FileHeaderContest
	:	public ReportSyntaxModel::FileHeaderContest
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	virtual void setContest(
			const std::string & _contestName
		,	CategoryContest::Enum _contest
	) = 0;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Writable
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HEADER_CONTEST_WRITABLE_HPP__
