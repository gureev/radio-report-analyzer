// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_ACCESSOR_IMPLEMENTATION_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_ACCESSOR_IMPLEMENTATION_HPP__

/*---------------------------------------------------------------------------*/

#include "radio_reports_analyzer/report_syntax_model/api/report_sm_accessor.hpp"

#include "framework/filesystem/api/fs_path_hasher.hpp"
#include "framework/filesystem/api/fs_path_comparator.hpp"

#include "framework/tools/common_headers/containers/hash_map.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample
{
	namespace FileSystem
	{
		class Accessor;
	}
}

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {

/*---------------------------------------------------------------------------*/

namespace Writable
{
	class File;
}

/*---------------------------------------------------------------------------*/

namespace Implementation {

/*---------------------------------------------------------------------------*/

class Accessor
	:	public ReportSyntaxModel::Accessor
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	Accessor( FileSystem::Accessor & _fileSystem );

	/*virtual*/ ~Accessor();

/*---------------------------------------------------------------------------*/

	/*virtual*/ const ReportSyntaxModel::File & getFile( boost::shared_ptr< FileSystem::Path > _path ) const;

	/*virtual*/ boost::optional< const ReportSyntaxModel::File & > findFile( boost::shared_ptr< FileSystem::Path > _path ) const;

/*---------------------------------------------------------------------------*/

	/*virtual*/ const ReportSyntaxModel::File & build( boost::shared_ptr< FileSystem::Path > _path );

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	Writable::File & createFile( boost::shared_ptr< FileSystem::Path > _path );

/*---------------------------------------------------------------------------*/

	FileSystem::Accessor & m_fileSystem;

	// TODO: Memory optimization
	// Replace
	//		boost::shared_ptr< File >
	// by
	//		File *
	typedef
		CommonHeaders::Containers::HashMap<
				boost::shared_ptr< FileSystem::Path >
			,	boost::shared_ptr< Writable::File >
			,	FileSystem::PathHasher
			,	FileSystem::PathComparator
		>
		Files;
	Files m_files;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_ACCESSOR_IMPLEMENTATION_HPP__
