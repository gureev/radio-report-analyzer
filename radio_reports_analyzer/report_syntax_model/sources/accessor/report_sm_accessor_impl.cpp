// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#include "radio_reports_analyzer/report_syntax_model/sources/ph/report_sm_ph.hpp"

#include "radio_reports_analyzer/report_syntax_model/api/report_sm_exceptions.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/accessor/report_sm_accessor_impl.hpp"
#include "radio_reports_analyzer/report_syntax_model/sources/analyzer/report_sm_analyzer.hpp"
#include "radio_reports_analyzer/report_syntax_model/sources/report_sm_file_impl.hpp"

#include "framework/exceptions/api/ex_exceptions.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

Accessor::Accessor( FileSystem::Accessor & _fileSystem )
	:	m_fileSystem( _fileSystem )
{
} // Accessor::Accessor

/*---------------------------------------------------------------------------*/

Accessor::~Accessor()
{
} // Accessor::~Accessor

/*---------------------------------------------------------------------------*/

const ReportSyntaxModel::File &
Accessor::getFile( boost::shared_ptr< FileSystem::Path > _path ) const
{
	auto file = m_files.getValue( _path );
	EX_DEBUG_ASSERT( file );

	return *( *file );

} // Accessor::getFile

/*---------------------------------------------------------------------------*/

boost::optional< const ReportSyntaxModel::File & >
Accessor::findFile( boost::shared_ptr< FileSystem::Path > _path ) const
{
	return *(*( m_files.getValue( _path ) ));

} // Accessor::findFile

/*---------------------------------------------------------------------------*/

Writable::File &
Accessor::createFile( boost::shared_ptr< FileSystem::Path > _path )
{
	if ( m_files.getValue( _path ) )
		EX_THROW_1_ARGUMENT(
				Exceptions::FileAlreadyAnalyzed
			,	_path
		);

	boost::shared_ptr< Writable::File > file(
		new Implementation::File( _path )
	);

	m_files[ _path ] = file;

	return * file;

} // Accessor::createFile

/*---------------------------------------------------------------------------*/

const ReportSyntaxModel::File &
Accessor::build( boost::shared_ptr< FileSystem::Path > _path )
{
	Writable::File & file = createFile( _path );

	Analyzer analyzer( m_fileSystem );
	analyzer.run( file );

	return file;

} // Accessor::build

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample
