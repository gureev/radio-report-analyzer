// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#include "radio_reports_analyzer/report_syntax_model/sources/ph/report_sm_ph.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/file_header/report_sm_file_header_address_impl.hpp"

#include "framework/exceptions/api/ex_exceptions.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

FileHeaderAddress::FileHeaderAddress()
{
} // FileHeaderAddress::FileHeaderAddress

/*---------------------------------------------------------------------------*/

FileHeaderAddress::~FileHeaderAddress()
{
} // FileHeaderAddress::~FileHeaderAddress

/*---------------------------------------------------------------------------*/

const std::string &
FileHeaderAddress::getAddress( int _index ) const
{
	EX_DEBUG_ASSERT(
			_index >= 0
		&&	_index < getAdressesCount()
	);
	return m_addresses[ _index ];

} // FileHeaderAddress::getAddress

/*---------------------------------------------------------------------------*/

int
FileHeaderAddress::getAdressesCount() const
{
	return m_addresses.size();

} // FileHeaderAddress::getAdressesCount

/*---------------------------------------------------------------------------*/

const std::string &
FileHeaderAddress::getCity() const
{
	return m_city;

} // FileHeaderAddress::getCity

/*---------------------------------------------------------------------------*/

const std::string &
FileHeaderAddress::getState() const
{
	return m_state;

} // FileHeaderAddress::getState

/*---------------------------------------------------------------------------*/

const std::string &
FileHeaderAddress::getPostalCode() const
{
	return m_postalCode;

} // FileHeaderAddress::getPostalCode

/*---------------------------------------------------------------------------*/

const std::string &
FileHeaderAddress::getCountry() const
{
	return m_country;

} // FileHeaderAddress::getCountry

/*---------------------------------------------------------------------------*/

void
FileHeaderAddress::pushAddress( const std::string & _address )
{
	m_addresses.push_back( _address );

} // FileHeaderAddress::pushAddress

/*---------------------------------------------------------------------------*/

void
FileHeaderAddress::setCity( const std::string & _city )
{
	EX_DEBUG_ASSERT( m_city.empty() );
	m_city = _city;

} // FileHeaderAddress::setCity

/*---------------------------------------------------------------------------*/

void
FileHeaderAddress::setState( const std::string & _state )
{
	EX_DEBUG_ASSERT( m_state.empty() );
	m_state = _state;

} // FileHeaderAddress::setState

/*---------------------------------------------------------------------------*/

void
FileHeaderAddress::setPostalCode( const std::string & _postalCode )
{
	EX_DEBUG_ASSERT( m_postalCode.empty() );
	m_postalCode = _postalCode;

} // FileHeaderAddress::setPostalCode

/*---------------------------------------------------------------------------*/

void
FileHeaderAddress::setCountry( const std::string & _country )
{
	EX_DEBUG_ASSERT( m_country.empty() );
	m_country = _country;

} // FileHeaderAddress::setCountry

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample
