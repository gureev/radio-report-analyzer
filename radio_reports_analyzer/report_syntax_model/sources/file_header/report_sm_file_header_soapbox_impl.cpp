// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#include "radio_reports_analyzer/report_syntax_model/sources/ph/report_sm_ph.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/file_header/report_sm_file_header_soapbox_impl.hpp"

#include "framework/exceptions/api/ex_exceptions.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

FileHeaderSoapbox::FileHeaderSoapbox()
{
} // FileHeaderSoapbox::FileHeaderSoapbox

/*---------------------------------------------------------------------------*/

FileHeaderSoapbox::~FileHeaderSoapbox()
{
} // FileHeaderSoapbox::~FileHeaderSoapbox

/*---------------------------------------------------------------------------*/

const std::string &
FileHeaderSoapbox::getLine( int _index ) const
{
	EX_DEBUG_ASSERT(
				_index >= 0
			&&	_index < getLinesCount()
	);
	return m_lines[ _index ];

} // FileHeaderSoapbox::getLine

/*---------------------------------------------------------------------------*/

int
FileHeaderSoapbox::getLinesCount() const
{
	return m_lines.size();

} // FileHeaderSoapbox::getLinesCount

/*---------------------------------------------------------------------------*/

void
FileHeaderSoapbox::pushLine( const std::string & _line )
{
	m_lines.push_back( _line );

} // FileHeaderSoapbox::pushLine

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample
