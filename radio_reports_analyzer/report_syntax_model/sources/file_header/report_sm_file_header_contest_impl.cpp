// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#include "radio_reports_analyzer/report_syntax_model/sources/ph/report_sm_ph.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/file_header/report_sm_file_header_contest_impl.hpp"

#include "framework/exceptions/api/ex_exceptions.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

FileHeaderContest::FileHeaderContest()
	:	m_contest( CategoryContest::Undefined )
{
} // FileHeaderContest::FileHeaderContest

/*---------------------------------------------------------------------------*/

FileHeaderContest::~FileHeaderContest()
{
} // FileHeaderContest::~FileHeaderContest

/*---------------------------------------------------------------------------*/

const std::string &
FileHeaderContest::getContestName() const
{
	return m_contestName;

} // FileHeaderContest::getContestName

/*---------------------------------------------------------------------------*/

CategoryContest::Enum
FileHeaderContest::getContest() const
{
	return m_contest;

} // FileHeaderContest::getContest

/*---------------------------------------------------------------------------*/

void
FileHeaderContest::setContest(
		const std::string & _contestName
	,	CategoryContest::Enum _contest
)
{
	EX_DEBUG_ASSERT( _contestName.empty() );
	EX_DEBUG_ASSERT( _contest == CategoryContest::Undefined );

	m_contestName = _contestName;
	m_contest = _contest;

} // FileHeaderContest::setContest

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample
