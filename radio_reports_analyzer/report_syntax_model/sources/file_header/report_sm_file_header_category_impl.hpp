// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HEADER_CATEGORY_IMPLEMENTATION_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HEADER_CATEGORY_IMPLEMENTATION_HPP__

/*---------------------------------------------------------------------------*/

#include "radio_reports_analyzer/report_syntax_model/writable/report_sm_file_header_category.hpp"

#include "framework/tools/common_headers/containers/vector.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class FileHeaderCategory
	:	public Writable::FileHeaderCategory
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	FileHeaderCategory();

	/*virtual*/ ~FileHeaderCategory();

/*---------------------------------------------------------------------------*/

	/*virtual*/ CategoryAssisted::Enum getAssisted() const;

	/*virtual*/ CategoryBand::Enum getBand() const;

	/*virtual*/ CategoryMode::Enum getMode() const;

	/*virtual*/ CategoryOperator::Enum getOperator() const;

	/*virtual*/ CategoryPower::Enum getPower() const;

	/*virtual*/ CategoryStation::Enum getStation() const;

	/*virtual*/ CategoryTime::Enum getTime() const;

	/*virtual*/ CategoryTransmitter::Enum getTransmitter() const;

	/*virtual*/ CategoryOverlay::Enum getOverlay() const;

/*---------------------------------------------------------------------------*/

	/*virtual*/ void setAssisted( CategoryAssisted::Enum _assisted );

	/*virtual*/ void setBand( CategoryBand::Enum _band );

	/*virtual*/ void setMode( CategoryMode::Enum _mode );

	/*virtual*/ void setOperator( CategoryOperator::Enum _operator );

	/*virtual*/ void setPower( CategoryPower::Enum _power );

	/*virtual*/ void setStation( CategoryStation::Enum _station );

	/*virtual*/ void setTime( CategoryTime::Enum _time );

	/*virtual*/ void setTransmitter( CategoryTransmitter::Enum _transmitter );

	/*virtual*/ void setOverlay( CategoryOverlay::Enum _overlay );

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	CategoryAssisted::Enum m_assisted;

	CategoryBand::Enum m_band;

	CategoryMode::Enum m_mode;

	CategoryOperator::Enum m_operator;

	CategoryPower::Enum m_power;

	CategoryStation::Enum m_station;

	CategoryTime::Enum m_time;

	CategoryTransmitter::Enum m_transmitter;

	CategoryOverlay::Enum m_overlay;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HEADER_CATEGORY_IMPLEMENTATION_HPP__
