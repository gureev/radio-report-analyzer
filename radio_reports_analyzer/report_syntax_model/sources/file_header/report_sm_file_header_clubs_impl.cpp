// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#include "radio_reports_analyzer/report_syntax_model/sources/ph/report_sm_ph.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/file_header/report_sm_file_header_clubs_impl.hpp"

#include "framework/exceptions/api/ex_exceptions.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

FileHeaderClubs::FileHeaderClubs()
{
} // FileHeaderClubs::FileHeaderClubs

/*---------------------------------------------------------------------------*/

FileHeaderClubs::~FileHeaderClubs()
{
} // FileHeaderClubs::~FileHeaderClubs

/*---------------------------------------------------------------------------*/

const std::string &
FileHeaderClubs::getClub( int _index ) const
{
	EX_DEBUG_ASSERT(
				_index >= 0
			&&	_index < getClubsCount()
	);
	return m_clubs[ _index ];

} // FileHeaderClubs::getClub

/*---------------------------------------------------------------------------*/

int
FileHeaderClubs::getClubsCount() const
{
	return m_clubs.size();

} // FileHeaderClubs::getClubsCount

/*---------------------------------------------------------------------------*/

void
FileHeaderClubs::pushClub( const std::string & _club )
{
	m_clubs.push_back( _club );

} // FileHeaderClubs::pushClub

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample
