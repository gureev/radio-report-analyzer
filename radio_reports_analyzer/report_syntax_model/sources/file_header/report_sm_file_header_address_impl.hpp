// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HEADER_ADDRESS_IMPLEMENTATION_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HEADER_ADDRESS_IMPLEMENTATION_HPP__

/*---------------------------------------------------------------------------*/

#include "radio_reports_analyzer/report_syntax_model/writable/report_sm_file_header_address.hpp"

#include "framework/tools/common_headers/containers/vector.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class FileHeaderAddress
	:	public Writable::FileHeaderAddress
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	FileHeaderAddress();

	/*virtual*/ ~FileHeaderAddress();

/*---------------------------------------------------------------------------*/

	/*virtual*/ const std::string & getAddress( int _index ) const;

	/*virtual*/ int getAdressesCount() const;

	/*virtual*/ const std::string & getCity() const;

	/*virtual*/ const std::string & getState() const;

	/*virtual*/ const std::string & getPostalCode() const;

	/*virtual*/ const std::string & getCountry() const;

/*---------------------------------------------------------------------------*/

	/*virtual*/ void pushAddress( const std::string & _address );

	/*virtual*/ void setCity( const std::string & _city );

	/*virtual*/ void setState( const std::string & _state );

	/*virtual*/ void setPostalCode( const std::string & _postalCode );

	/*virtual*/ void setCountry( const std::string & _country );

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	std::string m_city;

	std::string m_state;

	std::string m_postalCode;

	std::string m_country;

	typedef
		CommonHeaders::Containers::Vector< std::string >
		Addresses;
	Addresses m_addresses;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HEADER_ADDRESS_IMPLEMENTATION_HPP__
