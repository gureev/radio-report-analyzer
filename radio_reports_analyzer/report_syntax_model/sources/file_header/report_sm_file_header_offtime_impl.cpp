// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#include "radio_reports_analyzer/report_syntax_model/sources/ph/report_sm_ph.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/file_header/report_sm_file_header_offtime_impl.hpp"

#include "framework/exceptions/api/ex_exceptions.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

FileHeaderOfftime::FileHeaderOfftime()
{
} // FileHeaderOfftime::FileHeaderOfftime

/*---------------------------------------------------------------------------*/

FileHeaderOfftime::~FileHeaderOfftime()
{
} // FileHeaderOfftime::~FileHeaderOfftime

/*---------------------------------------------------------------------------*/

const Time &
FileHeaderOfftime::getBeginTime() const
{
	EX_DEBUG_ASSERT( m_beginTime );
	return * m_beginTime;

} // FileHeaderOfftime::getBeginTime

/*---------------------------------------------------------------------------*/

const Time &
FileHeaderOfftime::getEndTime() const
{
	EX_DEBUG_ASSERT( m_endTime );
	return * m_endTime;

} // FileHeaderOfftime::getEndTime

/*---------------------------------------------------------------------------*/

void
FileHeaderOfftime::setOfftime(
		std::auto_ptr< Time > _beginTime
	,	std::auto_ptr< Time > _endTime
)
{
	m_beginTime.reset( _beginTime.release() );
	m_endTime.reset( _endTime.release() );

} // FileHeaderOfftime::setOfftime

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample
