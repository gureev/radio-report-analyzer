// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#include "radio_reports_analyzer/report_syntax_model/sources/ph/report_sm_ph.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/file_header/report_sm_file_header_category_impl.hpp"

#include "framework/exceptions/api/ex_exceptions.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

FileHeaderCategory::FileHeaderCategory()
	:	m_assisted( CategoryAssisted::Undefined )
	,	m_band( CategoryBand::Undefined )
	,	m_mode( CategoryMode::Undefined )
	,	m_operator( CategoryOperator::Undefined )
	,	m_overlay( CategoryOverlay::Undefined )
	,	m_power( CategoryPower::Undefined )
	,	m_station( CategoryStation::Undefined )
	,	m_time( CategoryTime::Undefined )
	,	m_transmitter( CategoryTransmitter::Undefined )
{
} // FileHeaderCategory::FileHeaderCategory

/*---------------------------------------------------------------------------*/

FileHeaderCategory::~FileHeaderCategory()
{
} // FileHeaderCategory::~FileHeaderCategory

/*---------------------------------------------------------------------------*/

CategoryAssisted::Enum
FileHeaderCategory::getAssisted() const
{
	return m_assisted;

} // FileHeaderCategory::getAssisted

/*---------------------------------------------------------------------------*/

CategoryBand::Enum
FileHeaderCategory::getBand() const
{
	return m_band;

} // FileHeaderCategory::getBand

/*---------------------------------------------------------------------------*/

CategoryMode::Enum
FileHeaderCategory::getMode() const
{
	return m_mode;

} // FileHeaderCategory::getMode

/*---------------------------------------------------------------------------*/

CategoryOperator::Enum
FileHeaderCategory::getOperator() const
{
	return m_operator;

} // FileHeaderCategory::getOperator

/*---------------------------------------------------------------------------*/

CategoryPower::Enum
FileHeaderCategory::getPower() const
{
	return m_power;

} // FileHeaderCategory::getPower

/*---------------------------------------------------------------------------*/

CategoryStation::Enum
FileHeaderCategory::getStation() const
{
	return m_station;

} // FileHeaderCategory::getStation

/*---------------------------------------------------------------------------*/

CategoryTime::Enum
FileHeaderCategory::getTime() const
{
	return m_time;

} // FileHeaderCategory::getTime

/*---------------------------------------------------------------------------*/

CategoryTransmitter::Enum
FileHeaderCategory::getTransmitter() const
{
	return m_transmitter;

} // FileHeaderCategory::getTransmitter

/*---------------------------------------------------------------------------*/

CategoryOverlay::Enum
FileHeaderCategory::getOverlay() const
{
	return m_overlay;

} // FileHeaderCategory::getOverlay

/*---------------------------------------------------------------------------*/

void
FileHeaderCategory::setAssisted( CategoryAssisted::Enum _assisted )
{
	EX_DEBUG_ASSERT( m_assisted == CategoryAssisted::Undefined );
	m_assisted = _assisted;

} // FileHeaderCategory::setAssisted

/*---------------------------------------------------------------------------*/

void
FileHeaderCategory::setBand( CategoryBand::Enum _band )
{
	EX_DEBUG_ASSERT( m_band == CategoryBand::Undefined );
	m_band = _band;

} // FileHeaderCategory::setBand

/*---------------------------------------------------------------------------*/

void
FileHeaderCategory::setMode( CategoryMode::Enum _mode )
{
	EX_DEBUG_ASSERT( m_mode == CategoryMode::Undefined );
	m_mode = _mode;

} // FileHeaderCategory::setMode

/*---------------------------------------------------------------------------*/

void
FileHeaderCategory::setOperator( CategoryOperator::Enum _operator )
{
	EX_DEBUG_ASSERT( m_operator == CategoryOperator::Undefined );
	m_operator = _operator;

} // FileHeaderCategory::setOperator

/*---------------------------------------------------------------------------*/

void
FileHeaderCategory::setPower( CategoryPower::Enum _power )
{
	EX_DEBUG_ASSERT( m_power == CategoryPower::Undefined );
	m_power = _power;

} // FileHeaderCategory::setPower

/*---------------------------------------------------------------------------*/

void
FileHeaderCategory::setStation( CategoryStation::Enum _station )
{
	EX_DEBUG_ASSERT( m_station == CategoryStation::Undefined );
	m_station = _station;

} // FileHeaderCategory::setStation

/*---------------------------------------------------------------------------*/

void
FileHeaderCategory::setTime( CategoryTime::Enum _time )
{
	EX_DEBUG_ASSERT( m_time == CategoryTime::Undefined );
	m_time = _time;

} // FileHeaderCategory::setTime

/*---------------------------------------------------------------------------*/

void
FileHeaderCategory::setTransmitter( CategoryTransmitter::Enum _transmitter )
{
	EX_DEBUG_ASSERT( m_transmitter == CategoryTransmitter::Undefined );
	m_transmitter = _transmitter;

} // FileHeaderCategory::setTransmitter

/*---------------------------------------------------------------------------*/

void
FileHeaderCategory::setOverlay( CategoryOverlay::Enum _overlay )
{
	EX_DEBUG_ASSERT( m_overlay == CategoryOverlay::Undefined );
	m_overlay = _overlay;

} // FileHeaderCategory::setOverlay

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample
