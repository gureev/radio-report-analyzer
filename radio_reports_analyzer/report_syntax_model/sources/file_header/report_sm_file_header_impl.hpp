// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HEADER_IMPLEMENTATION_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HEADER_IMPLEMENTATION_HPP__

/*---------------------------------------------------------------------------*/

#include "radio_reports_analyzer/report_syntax_model/writable/report_sm_file_header_contest.hpp"
#include "radio_reports_analyzer/report_syntax_model/writable/report_sm_file_header_offtime.hpp"
#include "radio_reports_analyzer/report_syntax_model/writable/report_sm_file_header.hpp"
#include "radio_reports_analyzer/report_syntax_model/writable/report_sm_file_header_category.hpp"
#include "radio_reports_analyzer/report_syntax_model/writable/report_sm_file_header_clubs.hpp"
#include "radio_reports_analyzer/report_syntax_model/writable/report_sm_file_header_address.hpp"
#include "radio_reports_analyzer/report_syntax_model/writable/report_sm_file_header_soapbox.hpp"
#include "radio_reports_analyzer/report_syntax_model/writable/report_sm_file_header_operators.hpp"

#include "framework/tools/common_headers/version.hpp"
#include "framework/tools/common_headers/containers/vector.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class FileHeader
	:	public Writable::FileHeader
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	FileHeader();

	/*virtual*/ ~FileHeader();

/*---------------------------------------------------------------------------*/

	// START-OF-LOG:
	/*virtual*/ const CommonHeaders::Version & getLogVersion() const;

	// CLAIMED-SCORE:
	/*virtual*/ boost::optional< int > getClaimedScore() const;

	// CALLSIGN:
	/*virtual*/ boost::optional< const std::string & > getCallsign() const;

	// CREATED-BY:
	/*virtual*/ boost::optional< const std::string & > getCreatedBy() const;

	// EMAIL:
	/*virtual*/ boost::optional< const std::string & > getEmail() const;

	// LOCATION:
	/*virtual*/ boost::optional< const std::string & > getLocation() const;

	// NAME:
	/*virtual*/ boost::optional< const std::string & > getName() const;

	// CATEGORY-ASSISTED:
	// CATEGORY-BAND:
	// CATEGORY-MODE:
	// CATEGORY-OPERATOR:
	// CATEGORY-POWER:
	// CATEGORY-STATION:
	// CATEGORY-TIME:
	// CATEGORY-TRANSMITTER:
	// CATEGORY-OVERLAY:
	/*virtual*/ boost::optional< const ReportSyntaxModel::FileHeaderCategory & > getCategory() const;

	// ADDRESS:
	// ADDRESS-CITY:
	// ADDRESS-STATE-PROVINCE:
	// ADDRESS-POSTALCODE:
	// ADDRESS-COUNTRY:
	/*virtual*/ boost::optional< const ReportSyntaxModel::FileHeaderAddress & > getAddress() const;

	// CLUB:
	/*virtual*/ boost::optional< const ReportSyntaxModel::FileHeaderClubs & > getClubs() const;

	// CONTEST:
	/*virtual*/ boost::optional< const ReportSyntaxModel::FileHeaderContest & > getContest() const;

	// SOAPBOX:
	/*virtual*/ boost::optional< const ReportSyntaxModel::FileHeaderSoapbox & > getSoapbox() const;

	// OPERATORS:
	/*virtual*/ boost::optional< const ReportSyntaxModel::FileHeaderOperators & > getOperators() const;

	// OFFTIME:
	/*virtual*/ boost::optional< const ReportSyntaxModel::FileHeaderOfftime & > getOfftime() const;

/*---------------------------------------------------------------------------*/

	// START-OF-LOG:
	/*virtual*/ void setLogVersion( const CommonHeaders::Version & _version );

	// CALLSIGN:
	/*virtual*/ void setCallsign( const std::string & _callsign );

	// CLAIMED-SCORE:
	/*virtual*/ void setClaimedScore( int _claimedScore );

	// CREATED-BY:
	/*virtual*/ void setCreatedBy( const std::string & _createdBy );

	// EMAIL:
	/*virtual*/ void setEmail( const std::string & _email );

	// LOCATION:
	/*virtual*/ void setLocation( const std::string & _location );

	// NAME:
	/*virtual*/ void setName( const std::string & _name );

/*---------------------------------------------------------------------------*/

	// ADDRESS:
	/*virtual*/ void pushAddress( const std::string & _address );

	// ADDRESS-CITY:
	/*virtual*/ void setAddressCity( const std::string & _city );

	// ADDRESS-STATE-PROVINCE:
	/*virtual*/ void setAddressState( const std::string & _state );

	// ADDRESS-POSTALCODE:
	/*virtual*/ void setAddressPostalCode( const std::string & _postalCode );

	// ADDRESS-COUNTRY:
	/*virtual*/ void setAddressCountry( const std::string & _country );

/*---------------------------------------------------------------------------*/

	// CATEGORY-ASSISTED:
	/*virtual*/ void setCategoryAssisted( CategoryAssisted::Enum _assisted );

	// CATEGORY-BAND:
	/*virtual*/ void setCategoryBand( CategoryBand::Enum _band );

	// CATEGORY-MODE:
	/*virtual*/ void setCategoryMode( CategoryMode::Enum _mode );

	// CATEGORY-OPERATOR:
	/*virtual*/ void setCategoryOperator( CategoryOperator::Enum _operator );

	// CATEGORY-POWER:
	/*virtual*/ void setCategoryPower( CategoryPower::Enum _power );

	// CATEGORY-STATION:
	/*virtual*/ void setCategoryStation( CategoryStation::Enum _station );

	// CATEGORY-TIME:
	/*virtual*/ void setCategoryTime( CategoryTime::Enum _time );

	// CATEGORY-TRANSMITTER:
	/*virtual*/ void setCategoryTransmitter( CategoryTransmitter::Enum _transmitter );

	// CATEGORY-OVERLAY:
	/*virtual*/ void setCategoryOverlay( CategoryOverlay::Enum _overlay );

/*---------------------------------------------------------------------------*/

	// CLUB:
	/*virtual*/ void pushClub( const std::string & _club );

/*---------------------------------------------------------------------------*/

	// CONTEST:
	/*virtual*/ void setContest(
			const std::string & _contestName
		,	CategoryContest::Enum _contest
	);

/*---------------------------------------------------------------------------*/

	// OFFTIME:
	/*virtual*/ void setOfftime(
			std::auto_ptr< Time > _beginTime
		,	std::auto_ptr< Time > _endTime
	);

/*---------------------------------------------------------------------------*/

	// OPERATORS:
	/*virtual*/ void pushOperator( const std::string & _operator );

/*---------------------------------------------------------------------------*/

	// SOAPBOX:
	/*virtual*/ void pushSoapboxLine( const std::string & _line );

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	CommonHeaders::Version m_version;

	std::string m_callsign;

	int m_claimedScore;

	std::string m_createdBy;

	std::string m_email;

	std::string m_location;

	std::string m_name;

	boost::scoped_ptr< Writable::FileHeaderContest > m_contest;

	boost::scoped_ptr< Writable::FileHeaderOfftime > m_offtime;

	boost::scoped_ptr< Writable::FileHeaderCategory > m_category;

	boost::scoped_ptr< Writable::FileHeaderClubs > m_clubs;

	boost::scoped_ptr< Writable::FileHeaderAddress > m_address;

	boost::scoped_ptr< Writable::FileHeaderSoapbox > m_soapbox;

	boost::scoped_ptr< Writable::FileHeaderOperators > m_operators;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HEADER_IMPLEMENTATION_HPP__
