// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#include "radio_reports_analyzer/report_syntax_model/sources/ph/report_sm_ph.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/file_header/report_sm_file_header_operators_impl.hpp"

#include "framework/exceptions/api/ex_exceptions.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

FileHeaderOperators::FileHeaderOperators()
{
} // FileHeaderOperators::FileHeaderOperators

/*---------------------------------------------------------------------------*/

FileHeaderOperators::~FileHeaderOperators()
{
} // FileHeaderOperators::~FileHeaderOperators

/*---------------------------------------------------------------------------*/

const std::string &
FileHeaderOperators::getOperator( int _index ) const
{
	EX_DEBUG_ASSERT(
				_index >= 0
			&&	_index < getOperatorsCount()
	);
	return m_operators[ _index ];

} // FileHeaderOperators::getOperator

/*---------------------------------------------------------------------------*/

int
FileHeaderOperators::getOperatorsCount() const
{
	return m_operators.size();

} // FileHeaderOperators::getOperatorsCount

/*---------------------------------------------------------------------------*/

void
FileHeaderOperators::pushOperator( const std::string & _operator )
{
	m_operators.push_back( _operator );

} // FileHeaderOperators::pushOperator

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample
