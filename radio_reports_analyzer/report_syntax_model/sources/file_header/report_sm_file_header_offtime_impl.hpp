// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HEADER_OFFTIME_IMPLEMENTATION_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HEADER_OFFTIME_IMPLEMENTATION_HPP__

/*---------------------------------------------------------------------------*/

#include "radio_reports_analyzer/report_syntax_model/api/report_sm_time.hpp"

#include "radio_reports_analyzer/report_syntax_model/writable/report_sm_file_header_offtime.hpp"

#include "framework/tools/common_headers/containers/vector.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class FileHeaderOfftime
	:	public Writable::FileHeaderOfftime
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	FileHeaderOfftime();

	/*virtual*/ ~FileHeaderOfftime();

/*---------------------------------------------------------------------------*/

	/*virtual*/ const Time & getBeginTime() const;

	/*virtual*/ const Time & getEndTime() const;

/*---------------------------------------------------------------------------*/

	/*virtual*/ void setOfftime(
			std::auto_ptr< Time > _beginTime
		,	std::auto_ptr< Time > _endTime
	);

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	boost::scoped_ptr< Time > m_beginTime;

	boost::scoped_ptr< Time > m_endTime;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HEADER_OFFTIME_IMPLEMENTATION_HPP__
