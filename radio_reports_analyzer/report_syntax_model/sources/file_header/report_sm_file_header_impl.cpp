// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#include "radio_reports_analyzer/report_syntax_model/sources/ph/report_sm_ph.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/file_header/report_sm_file_header_impl.hpp"
#include "radio_reports_analyzer/report_syntax_model/sources/file_header/report_sm_file_header_address_impl.hpp"
#include "radio_reports_analyzer/report_syntax_model/sources/file_header/report_sm_file_header_category_impl.hpp"
#include "radio_reports_analyzer/report_syntax_model/sources/file_header/report_sm_file_header_clubs_impl.hpp"
#include "radio_reports_analyzer/report_syntax_model/sources/file_header/report_sm_file_header_contest_impl.hpp"
#include "radio_reports_analyzer/report_syntax_model/sources/file_header/report_sm_file_header_offtime_impl.hpp"
#include "radio_reports_analyzer/report_syntax_model/sources/file_header/report_sm_file_header_operators_impl.hpp"
#include "radio_reports_analyzer/report_syntax_model/sources/file_header/report_sm_file_header_soapbox_impl.hpp"

#include "framework/exceptions/api/ex_exceptions.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

FileHeader::FileHeader()
	:	m_claimedScore( -1 )
{
} // FileHeader::FileHeader

/*---------------------------------------------------------------------------*/

FileHeader::~FileHeader()
{
} // FileHeader::~FileHeader

/*---------------------------------------------------------------------------*/

const CommonHeaders::Version &
FileHeader::getLogVersion() const
{
	return m_version;

} // FileHeader::getLogVersion

/*---------------------------------------------------------------------------*/

boost::optional< int >
FileHeader::getClaimedScore() const
{
	return m_claimedScore != -1
		?	boost::optional< int >( m_claimedScore )
		:	boost::optional< int >()
	;

} // FileHeader::getClaimedScore

/*---------------------------------------------------------------------------*/

boost::optional< const std::string & >
FileHeader::getCallsign() const
{
	return m_callsign.empty()
		?	boost::optional< const std::string & >( m_callsign )
		:	boost::optional< const std::string & >()
	;

} // FileHeader::getCallsign

/*---------------------------------------------------------------------------*/

boost::optional< const std::string & >
FileHeader::getCreatedBy() const
{
	return m_createdBy.empty()
		?	boost::optional< const std::string & >( m_createdBy )
		:	boost::optional< const std::string & >()
	;

} // FileHeader::getCreatedBy

/*---------------------------------------------------------------------------*/

boost::optional< const std::string & >
FileHeader::getEmail() const
{
	return m_email.empty()
		?	boost::optional< const std::string & >( m_email )
		:	boost::optional< const std::string & >()
	;

} // FileHeader::getEmail

/*---------------------------------------------------------------------------*/

boost::optional< const std::string & >
FileHeader::getLocation() const
{
	return m_location.empty()
		?	boost::optional< const std::string & >( m_location )
		:	boost::optional< const std::string & >()
	;

} // FileHeader::getLocation

/*---------------------------------------------------------------------------*/

boost::optional< const std::string & >
FileHeader::getName() const
{
	return m_name.empty()
		?	boost::optional< const std::string & >( m_name )
		:	boost::optional< const std::string & >()
	;

} // FileHeader::getName

/*---------------------------------------------------------------------------*/

boost::optional< const ReportSyntaxModel::FileHeaderCategory & >
FileHeader::getCategory() const
{
	return m_category
		?	boost::optional< const ReportSyntaxModel::FileHeaderCategory & >( * m_category )
		:	boost::optional< const ReportSyntaxModel::FileHeaderCategory & >()
	;

} // FileHeader::getCategory

/*---------------------------------------------------------------------------*/

boost::optional< const ReportSyntaxModel::FileHeaderAddress & >
FileHeader::getAddress() const
{
	return m_address
		?	boost::optional< const ReportSyntaxModel::FileHeaderAddress & >( * m_address )
		:	boost::optional< const ReportSyntaxModel::FileHeaderAddress & >()
	;

} // FileHeader::getAddress

/*---------------------------------------------------------------------------*/

boost::optional< const ReportSyntaxModel::FileHeaderClubs & >
FileHeader::getClubs() const
{
	return m_clubs
		?	boost::optional< const ReportSyntaxModel::FileHeaderClubs & >( * m_clubs )
		:	boost::optional< const ReportSyntaxModel::FileHeaderClubs & >()
	;

} // FileHeader::getClubs

/*---------------------------------------------------------------------------*/

boost::optional< const ReportSyntaxModel::FileHeaderContest & >
FileHeader::getContest() const
{
	return m_contest
		?	boost::optional< const ReportSyntaxModel::FileHeaderContest & >( * m_contest )
		:	boost::optional< const ReportSyntaxModel::FileHeaderContest & >()
	;

} // FileHeader::getContest

/*---------------------------------------------------------------------------*/

boost::optional< const ReportSyntaxModel::FileHeaderSoapbox & >
FileHeader::getSoapbox() const
{
	return m_soapbox
		?	boost::optional< const ReportSyntaxModel::FileHeaderSoapbox & >( * m_soapbox )
		:	boost::optional< const ReportSyntaxModel::FileHeaderSoapbox & >()
	;

} // FileHeader::getSoapbox

/*---------------------------------------------------------------------------*/

boost::optional< const ReportSyntaxModel::FileHeaderOperators & >
FileHeader::getOperators() const
{
	return m_operators
		?	boost::optional< const ReportSyntaxModel::FileHeaderOperators & >( * m_operators )
		:	boost::optional< const ReportSyntaxModel::FileHeaderOperators & >()
	;

} // FileHeader::getOperators

/*---------------------------------------------------------------------------*/

boost::optional< const ReportSyntaxModel::FileHeaderOfftime & >
FileHeader::getOfftime() const
{
	return m_offtime
		?	boost::optional< const ReportSyntaxModel::FileHeaderOfftime & >( * m_offtime )
		:	boost::optional< const ReportSyntaxModel::FileHeaderOfftime & >()
	;

} // FileHeader::getOfftime

/*---------------------------------------------------------------------------*/

void
FileHeader::setLogVersion( const CommonHeaders::Version & _version )
{
	m_version = _version;

} // FileHeader::setLogVersion

/*---------------------------------------------------------------------------*/

void
FileHeader::setCallsign( const std::string & _callsign )
{
	EX_DEBUG_ASSERT( m_callsign.empty() );
	m_callsign = _callsign;

} // FileHeader::setCallsign

/*---------------------------------------------------------------------------*/

void
FileHeader::setClaimedScore( int _claimedScore )
{
	EX_DEBUG_ASSERT( !_claimedScore );
	m_claimedScore = _claimedScore;

} // FileHeader::setClaimedScore

/*---------------------------------------------------------------------------*/

void
FileHeader::setCreatedBy( const std::string & _createdBy )
{
	EX_DEBUG_ASSERT( m_createdBy.empty() );
	m_createdBy = _createdBy;

} // FileHeader::setCreatedBy

/*---------------------------------------------------------------------------*/

void
FileHeader::setEmail( const std::string & _email )
{
	EX_DEBUG_ASSERT( m_email.empty() );
	m_email = _email;

} // FileHeader::setEmail

/*---------------------------------------------------------------------------*/

void
FileHeader::setLocation( const std::string & _location )
{
	EX_DEBUG_ASSERT( m_location.empty() );
	m_location = _location;

} // FileHeader::setLocation

/*---------------------------------------------------------------------------*/

void
FileHeader::setName( const std::string & _name )
{
	EX_DEBUG_ASSERT( m_name.empty() );
	m_name = _name;

} // FileHeader::setName

/*---------------------------------------------------------------------------*/

void
FileHeader::pushAddress( const std::string & _address )
{
	if ( ! m_address )
		m_address.reset( new FileHeaderAddress );

	EX_DEBUG_ASSERT( m_address );
	m_address->pushAddress( _address );

} // FileHeader::pushAddress

/*---------------------------------------------------------------------------*/

void
FileHeader::setAddressCity( const std::string & _city )
{
	if ( ! m_address )
		m_address.reset( new FileHeaderAddress );

	EX_DEBUG_ASSERT( m_address );
	m_address->setCity( _city );

} // FileHeader::setAddressCity

/*---------------------------------------------------------------------------*/

void
FileHeader::setAddressState( const std::string & _state )
{
	if ( ! m_address )
		m_address.reset( new FileHeaderAddress );

	EX_DEBUG_ASSERT( m_address );
	m_address->setState( _state );

} // FileHeader::setAddressState

/*---------------------------------------------------------------------------*/

void
FileHeader::setAddressPostalCode( const std::string & _postalCode )
{
	if ( ! m_address )
		m_address.reset( new FileHeaderAddress );

	EX_DEBUG_ASSERT( m_address );
	m_address->setPostalCode( _postalCode );

} // FileHeader::setAddressPostalCode

/*---------------------------------------------------------------------------*/

void
FileHeader::setAddressCountry( const std::string & _country )
{
	if ( ! m_address )
		m_address.reset( new FileHeaderAddress );

	EX_DEBUG_ASSERT( m_address );
	m_address->setCountry( _country );

} // FileHeader::setAddressCountry

/*---------------------------------------------------------------------------*/

void
FileHeader::setCategoryAssisted( CategoryAssisted::Enum _assisted )
{
	if ( ! m_category )
		m_category.reset( new FileHeaderCategory );

	EX_DEBUG_ASSERT( m_category );
	m_category->setAssisted( _assisted );

} // FileHeader::setCategoryAssisted

/*---------------------------------------------------------------------------*/

void
FileHeader::setCategoryBand( CategoryBand::Enum _band )
{
	if ( ! m_category )
		m_category.reset( new FileHeaderCategory );

	EX_DEBUG_ASSERT( m_category );
	m_category->setBand( _band );

} // FileHeader::setCategoryBand

/*---------------------------------------------------------------------------*/

void
FileHeader::setCategoryMode( CategoryMode::Enum _mode )
{
	if ( ! m_category )
		m_category.reset( new FileHeaderCategory );

	EX_DEBUG_ASSERT( m_category );
	m_category->setMode( _mode );

} // FileHeader::setCategoryMode

/*---------------------------------------------------------------------------*/

void
FileHeader::setCategoryOperator( CategoryOperator::Enum _operator )
{
	if ( ! m_category )
		m_category.reset( new FileHeaderCategory );

	EX_DEBUG_ASSERT( m_category );
	m_category->setOperator( _operator );

} // FileHeader::setCategoryOperator

/*---------------------------------------------------------------------------*/

void
FileHeader::setCategoryPower( CategoryPower::Enum _power )
{
	if ( ! m_category )
		m_category.reset( new FileHeaderCategory );

	EX_DEBUG_ASSERT( m_category );
	m_category->setPower( _power );

} // FileHeader::setCategoryPower

/*---------------------------------------------------------------------------*/

void
FileHeader::setCategoryStation( CategoryStation::Enum _station )
{
	if ( ! m_category )
		m_category.reset( new FileHeaderCategory );

	EX_DEBUG_ASSERT( m_category );
	m_category->setStation( _station );

} // FileHeader::setCategoryStation

/*---------------------------------------------------------------------------*/

void
FileHeader::setCategoryTime( CategoryTime::Enum _time )
{
	if ( ! m_category )
		m_category.reset( new FileHeaderCategory );

	EX_DEBUG_ASSERT( m_category );
	m_category->setTime( _time );

} // FileHeader::setCategoryTime

/*---------------------------------------------------------------------------*/

void
FileHeader::setCategoryTransmitter( CategoryTransmitter::Enum _transmitter )
{
	if ( ! m_category )
		m_category.reset( new FileHeaderCategory );

	EX_DEBUG_ASSERT( m_category );
	m_category->setTransmitter( _transmitter );

} // FileHeader::setCategoryTransmitter

/*---------------------------------------------------------------------------*/

void
FileHeader::setCategoryOverlay( CategoryOverlay::Enum _overlay )
{
	if ( ! m_category )
		m_category.reset( new FileHeaderCategory );

	EX_DEBUG_ASSERT( m_category );
	m_category->setOverlay( _overlay );

} // FileHeader::setCategoryOverlay

/*---------------------------------------------------------------------------*/

void
FileHeader::pushClub( const std::string & _club )
{
	if ( ! m_clubs )
		m_clubs.reset( new FileHeaderClubs );

	EX_DEBUG_ASSERT( m_clubs );
	m_clubs->pushClub( _club );

} // FileHeader::pushClub


/*---------------------------------------------------------------------------*/

void
FileHeader::setContest(
		const std::string & _contestName
	,	CategoryContest::Enum _contest
)
{
	if ( ! m_contest )
		m_contest.reset( new FileHeaderContest );

	EX_DEBUG_ASSERT( m_contest );
	m_contest->setContest(
			_contestName
		,	_contest
	);

} // FileHeader::setContest

/*---------------------------------------------------------------------------*/

void
FileHeader::setOfftime(
		std::auto_ptr< Time > _beginTime
	,	std::auto_ptr< Time > _endTime
)
{
	if ( ! m_offtime )
		m_offtime.reset( new FileHeaderOfftime );

	EX_DEBUG_ASSERT( m_offtime );
	m_offtime->setOfftime(
			_beginTime
		,	_endTime
	);

} // FileHeader::setOfftime

/*---------------------------------------------------------------------------*/

void
FileHeader::pushOperator( const std::string & _operator )
{
	if ( ! m_operators )
		m_operators.reset( new FileHeaderOperators );

	EX_DEBUG_ASSERT( m_operators );
	m_operators->pushOperator( _operator );

} // FileHeader::pushOperator

/*---------------------------------------------------------------------------*/

void
FileHeader::pushSoapboxLine( const std::string & _line )
{
	if ( ! m_soapbox )
		m_soapbox.reset( new FileHeaderSoapbox );

	EX_DEBUG_ASSERT( m_soapbox );
	m_soapbox->pushLine( _line );

} // FileHeader::pushSoapboxLine

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample
