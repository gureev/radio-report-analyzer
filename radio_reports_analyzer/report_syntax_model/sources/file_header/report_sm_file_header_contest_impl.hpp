// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HEADER_CONTEST_IMPLEMENTATION_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HEADER_CONTEST_IMPLEMENTATION_HPP__

/*---------------------------------------------------------------------------*/

#include "radio_reports_analyzer/report_syntax_model/writable/report_sm_file_header_contest.hpp"

#include "framework/tools/common_headers/containers/vector.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class FileHeaderContest
	:	public Writable::FileHeaderContest
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	FileHeaderContest();

	/*virtual*/ ~FileHeaderContest();

/*---------------------------------------------------------------------------*/

	/*virtual*/ const std::string & getContestName() const;

	/*virtual*/ CategoryContest::Enum getContest() const;

/*---------------------------------------------------------------------------*/

	/*virtual*/ void setContest(
			const std::string & _contestName
		,	CategoryContest::Enum _contest
	);

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	std::string m_contestName;

	CategoryContest::Enum m_contest;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HEADER_CONTEST_IMPLEMENTATION_HPP__
