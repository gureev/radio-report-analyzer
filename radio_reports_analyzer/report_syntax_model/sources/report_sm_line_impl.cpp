// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#include "radio_reports_analyzer/report_syntax_model/sources/ph/report_sm_ph.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/report_sm_line_impl.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

Line::Line(
		LineCodeKind::Enum _codeKind
	,	int _frequency
	,	const std::string & _mode
	,	std::auto_ptr< ReportSyntaxModel::Time > _time
)
	:	m_codeKind( _codeKind )
	,	m_frequency( _frequency )
	,	m_mode( _mode )
	,	m_time( _time )
{
} // Line::Line

/*---------------------------------------------------------------------------*/

Line::~Line()
{
} // Line::~Line

/*---------------------------------------------------------------------------*/

LineCodeKind::Enum
Line::getCodeKind() const
{
	return m_codeKind;

} // Line::getCodeKind

/*---------------------------------------------------------------------------*/

int
Line::getFrequency() const
{
	return m_frequency;

} // Line::getFrequency

/*---------------------------------------------------------------------------*/

const std::string &
Line::getMode() const
{
	return m_mode;

} // Line::getMode

/*---------------------------------------------------------------------------*/

const Time &
Line::getTime() const
{
	assert( m_time );
	return * m_time;

} // Line::getTime

/*---------------------------------------------------------------------------*/

const Operator &
Line::getSent() const
{
	assert( m_sent );
	return * m_sent;

} // Line::getSent

/*---------------------------------------------------------------------------*/

const Operator &
Line::getReceived() const
{
	assert( m_received );
	return * m_received;

} // Line::getReceived

/*---------------------------------------------------------------------------*/

void
Line::setSent( std::auto_ptr< Operator > _operator )
{
	assert( ! m_sent );
	m_sent.reset( _operator.release() );

} // Line::setSent

/*---------------------------------------------------------------------------*/

void
Line::setReceived( std::auto_ptr< Operator > _operator )
{
	assert( ! m_received );
	m_received.reset( _operator.release() );

} // Line::setReceived

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample
