// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#include "radio_reports_analyzer/report_syntax_model/sources/ph/report_sm_ph.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/report_sm_factory.hpp"
#include "radio_reports_analyzer/report_syntax_model/sources/report_sm_line_impl.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

boost::scoped_ptr< Factory > Factory::m_factory;

/*---------------------------------------------------------------------------*/

Factory &
Factory::getInstance()
{
	if ( ! m_factory )
		m_factory.reset( new Factory() );

	return * m_factory;

} // Factory::getInstance

/*---------------------------------------------------------------------------*/

std::auto_ptr< Writable::Line >
Factory::createLine(
		LineCodeKind::Enum _codeKind
	,	int _frequency
	,	const std::string & _mode
	,	std::auto_ptr< Time > _time
)
{
	return std::auto_ptr< Writable::Line >(
		new Implementation::Line(
				_codeKind
			,	_frequency
			,	_mode
			,	_time
		)
	);

} // Factory::createLine

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample
