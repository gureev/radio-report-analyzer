// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#include "radio_reports_analyzer/report_syntax_model/sources/ph/report_sm_ph.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/report_sm_time_impl.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

Time::Time(
		int _year
	,	int _month
	,	int _day
	,	int _hours
	,	int _minutes
)
	:	m_year( _year )
	,	m_month( _month )
	,	m_day( _day )
	,	m_hours( _hours )
	,	m_minutes( _minutes )
{
} // Time::Time

/*---------------------------------------------------------------------------*/

Time::~Time()
{
} // Time::~Time

/*---------------------------------------------------------------------------*/

int
Time::getMinutes() const
{
	return m_minutes;

} // Time::getMinutes

/*---------------------------------------------------------------------------*/

int
Time::getHours() const
{
	return m_hours;

} // Time::getHours

/*---------------------------------------------------------------------------*/

int
Time::getDay() const
{
	return m_day;

} // Time::getDay

/*---------------------------------------------------------------------------*/

int
Time::getMonth() const
{
	return m_month;

} // Time::getMonth

/*---------------------------------------------------------------------------*/

int
Time::getYear() const
{
	return m_year;

} // Time::getYear

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample
