// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_LINE_IMPLEMENTATION_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_LINE_IMPLEMENTATION_HPP__

/*---------------------------------------------------------------------------*/

#include "radio_reports_analyzer/report_syntax_model/api/report_sm_operator.hpp"
#include "radio_reports_analyzer/report_syntax_model/api/report_sm_time.hpp"

#include "radio_reports_analyzer/report_syntax_model/writable/report_sm_line.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class Line
	:	public Writable::Line
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	Line(
			LineCodeKind::Enum _codeKind
		,	int _frequency
		,	const std::string & _mode
		,	std::auto_ptr< ReportSyntaxModel::Time > _time
	);

	/*virtual*/ ~Line();

/*---------------------------------------------------------------------------*/

	/*virtual*/ LineCodeKind::Enum getCodeKind() const;

/*---------------------------------------------------------------------------*/

	/*virtual*/ int getFrequency() const;

	/*virtual*/ const std::string & getMode() const;

	/*virtual*/ const Time & getTime() const;

/*---------------------------------------------------------------------------*/

	/*virtual*/ const Operator & getSent() const;

	/*virtual*/ const Operator & getReceived() const;

/*---------------------------------------------------------------------------*/

	/*virtual*/ void setSent( std::auto_ptr< Operator > _operator );

	/*virtual*/ void setReceived( std::auto_ptr< Operator > _operator );

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	LineCodeKind::Enum m_codeKind;

	int m_frequency;

	std::string m_mode;

	boost::scoped_ptr< Time > m_time;

	boost::scoped_ptr< Operator > m_sent;

	boost::scoped_ptr< Operator > m_received;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_LINE_IMPLEMENTATION_HPP__
