// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#include "radio_reports_analyzer/report_syntax_model/sources/ph/report_sm_ph.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/analyzer/report_sm_builder.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/report_sm_file_impl.hpp"
#include "radio_reports_analyzer/report_syntax_model/sources/file_header/report_sm_file_header_impl.hpp"
#include "radio_reports_analyzer/report_syntax_model/api/report_sm_exceptions.hpp"

#include "framework/tools/common_headers/version.hpp"

#include <boost/lexical_cast.hpp>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

Builder::Builder(
		FileSystem::Accessor & _fileSystem
	,	Writable::File & _file
)
	:	m_fileSystem( _fileSystem )
	,	m_file( _file )
	,	m_header( new Implementation::FileHeader() )
{
} // Builder::Builder

/*---------------------------------------------------------------------------*/

Builder::~Builder()
{
} // Builder::~Builder

/*---------------------------------------------------------------------------*/

const ReportSyntaxModel::File &
Builder::getFile() const
{
	return m_file;

} // Builder::getFile

/*---------------------------------------------------------------------------*/

void
Builder::setLogVersion( const char * _version )
{
	CommonHeaders::Version version = CommonHeaders::Version::fromString( _version );

	if ( ! version.isValid() )
		EX_THROW_1_ARGUMENT(
				Exceptions::InvalidLogVersion
			,	m_file.getPath()
		);

	m_header->setLogVersion( version );

} // Builder::setLogVersion

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample
