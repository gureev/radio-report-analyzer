
%option noyywrap
%option stack

%TOP{

 /*---------------------------------------------------------------------------*/

#include <QtCore/qbuffer.h>

#include "radio_reports_analyzer/report_syntax_model/api/report_sm_exceptions.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/analyzer/report_sm_builder.hpp"

 /*---------------------------------------------------------------------------*/

#define SOURCE_TYPE QIODevice

int QIODeviceInput(
		QIODevice * _inputStream
	,	char * _buffer
	,	int _maxSize
)
{
	if ( _inputStream->atEnd() )
		return 0;

	return static_cast< int >(
		_inputStream->read(
				_buffer
			,	_maxSize
		)
	);

} // QIODeviceInput

#define YY_INPUT( _buffer, _result, _maxSize )\
	if ( ( _result = QIODeviceInput( yyin, ( char * ) _buffer, _maxSize ) ) < 0 )\
		throw std::exception();\

#define ECHO 0
#define STDIN 0
#define STDOUT 0

 /*---------------------------------------------------------------------------*/

}

%{

 /*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

 /*---------------------------------------------------------------------------*/

Builder * g_builder;

 /*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

 /*---------------------------------------------------------------------------*/

%}

 /*---------------------------------------------------------------------------*/
 /* Start conditions                                                          */
 /*---------------------------------------------------------------------------*/

%x START_OF_FILE

%x START_OF_LOG_Found

 /*---------------------------------------------------------------------------*/
 /* Name definitions                                                          */
 /*---------------------------------------------------------------------------*/

Space								[ \t]
Delimiter							":"

LinuxEOL							"\n"
WindowsEOL							"\r\n"
EOL									{WindowsEOL}|{LinuxEOL}

IntegerNumber						([0-9])+
DoubleNumber						{IntegerNumber}"."{IntegerNumber}

 /*---------------------------------------------------------------------------*/

%%

 /*---------------------------------------------------------------------------*/

	using namespace Ample::RadioReportsAnalyzer::ReportSyntaxModel;
	using namespace Ample::RadioReportsAnalyzer::ReportSyntaxModel::Implementation;

 /*---------------------------------------------------------------------------*/
<START_OF_FILE>{
 /*---------------------------------------------------------------------------*/

	"START-OF-LOG"					{
										yy_push_state( START_OF_LOG_Found );
									}

} // START_OF_FILE

 /*---------------------------------------------------------------------------*/
<START_OF_LOG_Found>{
 /*---------------------------------------------------------------------------*/

	{IntegerNumber}					{
										g_builder->setLogVersion( yytext );
										yy_pop_state();
									}

	{EOL}							|
	<<EOF>>							{
										EX_THROW_1_ARGUMENT(
												Exceptions::InvalidLogVersion
											,	g_builder->getFile().getPath()
										);
									}

} // START_OF_LOG_Found

 /*---------------------------------------------------------------------------*/
<START_OF_FILE>{
 /*---------------------------------------------------------------------------*/

	{Delimiter}						|
	{Space}+						{
										// Just skip
									}

} // START_OF_FILE

 /*---------------------------------------------------------------------------*/

%%
