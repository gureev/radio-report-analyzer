// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#include "radio_reports_analyzer/report_syntax_model/sources/ph/report_sm_ph.hpp"

#include "radio_reports_analyzer/report_syntax_model/api/report_sm_exceptions.hpp"

#include "radio_reports_analyzer/report_syntax_model/writable/report_sm_file.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/analyzer/report_sm_analyzer.hpp"
#include "radio_reports_analyzer/report_syntax_model/sources/analyzer/report_sm_builder.hpp"
#include "radio_reports_analyzer/report_syntax_model/sources/analyzer/report_sm_lexer.hpp"

#include "framework/filesystem/api/fs_accessor.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

Analyzer::Analyzer( FileSystem::Accessor & _fileSystem )
	:	m_fileSystem( _fileSystem )
{
} // Analyzer::Analyzer

/*---------------------------------------------------------------------------*/

Analyzer::~Analyzer()
{
} // Analyzer::~Analyzer

/*---------------------------------------------------------------------------*/

void
Analyzer::run( Writable::File & _file ) const
{
	const FileSystem::Path & filePath = * _file.getPath();

	bool canAnalyze =
			m_fileSystem.exists( filePath )
		&&	m_fileSystem.isFile( filePath )
		&&	m_fileSystem.canRead( filePath )
	;

	if ( ! canAnalyze )
		EX_THROW_1_ARGUMENT(
				Exceptions::CannotAnalyzeFile
			,	_file.getPath()
		);

	boost::shared_ptr< FileSystem::FileHandle > fileHandle =
		m_fileSystem.openFile(
				_file.getPath()
			,	FileSystem::OpenMode::Read
		);

	Builder builder(
			m_fileSystem
		,	_file
	);

	g_builder = & builder;

} // Analyzer::run

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample
