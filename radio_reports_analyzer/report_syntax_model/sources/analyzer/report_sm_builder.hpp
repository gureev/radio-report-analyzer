// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_BUILDER_IMPLEMENTATION_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_BUILDER_IMPLEMENTATION_HPP__

/*---------------------------------------------------------------------------*/

#include "radio_reports_analyzer/report_syntax_model/writable/report_sm_file.hpp"
#include "radio_reports_analyzer/report_syntax_model/writable/report_sm_file_header.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample
{
	namespace FileSystem
	{
		class Accessor;
	}
}

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class Builder
	:	public boost::noncopyable
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	Builder(
			FileSystem::Accessor & _fileSystem
		,	Writable::File & _file
	);

	virtual ~Builder();

/*---------------------------------------------------------------------------*/

	const ReportSyntaxModel::File & getFile() const;

/*---------------------------------------------------------------------------*/

	void setLogVersion( const char * _version );

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	FileSystem::Accessor & m_fileSystem;

	Writable::File & m_file;

	std::auto_ptr< Writable::FileHeader > m_header;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_BUILDER_IMPLEMENTATION_HPP__
