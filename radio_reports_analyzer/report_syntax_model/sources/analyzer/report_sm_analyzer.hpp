// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_ANALYZER_IMPLEMENTATION_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_ANALYZER_IMPLEMENTATION_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample
{
	namespace FileSystem
	{
		class Accessor;
	}

	namespace RadioReportsAnalyzer
	{
		namespace ReportSyntaxModel
		{
			namespace Writable
			{
				class File;
			}
		}
	}
}

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class Analyzer
	:	public boost::noncopyable
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	Analyzer( FileSystem::Accessor & _fileSystem );

	virtual ~Analyzer();

/*---------------------------------------------------------------------------*/

	void run( Writable::File & _file ) const;

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	FileSystem::Accessor & m_fileSystem;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_ANALYZER_IMPLEMENTATION_HPP__
