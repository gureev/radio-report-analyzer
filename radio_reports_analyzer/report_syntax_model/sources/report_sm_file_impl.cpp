// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#include "radio_reports_analyzer/report_syntax_model/sources/ph/report_sm_ph.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/report_sm_file_impl.hpp"
#include "radio_reports_analyzer/report_syntax_model/sources/report_sm_factory.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/file_header/report_sm_file_header_impl.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

File::File( boost::shared_ptr< FileSystem::Path > _filePath )
	:	m_filePath( _filePath )
{
} // File::File

/*---------------------------------------------------------------------------*/

File::~File()
{
} // File::~File

/*---------------------------------------------------------------------------*/

boost::shared_ptr< FileSystem::Path >
File::getPath() const
{
	return m_filePath;

} // File::getPath

/*---------------------------------------------------------------------------*/

boost::optional< const ReportSyntaxModel::FileHeader & >
File::getHeader() const
{
	return m_header
		?	boost::optional< const ReportSyntaxModel::FileHeader & >( * m_header )
		:	boost::optional< const ReportSyntaxModel::FileHeader & >()
	;

} // File::getHeader

/*---------------------------------------------------------------------------*/

int
File::getLinesCount() const
{
	return m_lines.size();

} // File::getLinesCount

/*---------------------------------------------------------------------------*/

const Line &
File::getLine( int _index ) const
{
	assert(	_index >= 0 && _index < m_lines.size() );
	assert( m_lines[ _index ] );
	return * m_lines[ _index ];

} // File::getLine

/*---------------------------------------------------------------------------*/

void
File::setHeader( std::auto_ptr< Writable::FileHeader > _fileHeader )
{
	assert( ! m_header );
	m_header.reset( _fileHeader.release() );

} // File::setHeader

/*---------------------------------------------------------------------------*/

Writable::Line &
File::pushLine(
		LineCodeKind::Enum _codeKind
	,	int _frequency
	,	const std::string & _mode
	,	std::auto_ptr< Time > _time
)
{
	boost::shared_ptr< Writable::Line > line(
		Factory::getInstance().createLine(
				_codeKind
			,	_frequency
			,	_mode
			,	_time
		)
	);

	m_lines.push_back( line );

	return * line;

} // File::pushLine

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample
