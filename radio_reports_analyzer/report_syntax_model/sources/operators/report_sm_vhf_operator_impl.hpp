// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_VHF_OPERATOR_IMPLEMENTATION_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_VHF_OPERATOR_IMPLEMENTATION_HPP__

/*---------------------------------------------------------------------------*/

#include "radio_reports_analyzer/report_syntax_model/api/report_sm_vhf_operator.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/operators/report_sm_operator_base_impl.hpp"

#include "framework/tools/common_headers/containers/vector.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class VhfOperator
	:	public OperatorBase< ReportSyntaxModel::VhfOperator >
{

/*---------------------------------------------------------------------------*/

	typedef
		OperatorBase< ReportSyntaxModel::VhfOperator >
		BaseOperator;

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	VhfOperator(
			const ReportSyntaxModel::Line & _line
		,	const std::string & _callsign
		,	const std::string & _grid
	);

	/*virtual*/ ~VhfOperator();

/*---------------------------------------------------------------------------*/

	/*virtual*/ const std::string & getGrid() const;

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	std::string m_grid;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_VHF_OPERATOR_IMPLEMENTATION_HPP__
