// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#include "radio_reports_analyzer/report_syntax_model/sources/ph/report_sm_ph.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/operators/report_sm_rtty_operator_impl.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

RttyOperator::RttyOperator(
		const ReportSyntaxModel::Line & _line
	,	const std::string & _callsign
	,	int _report
	,	int _serialNumber
	,	int _hours
	,	int _minutes
)
	:	BaseOperator(
				_line
			,	_callsign
		)
	,	m_report( _report )
	,	m_serialNumber( _serialNumber )
	,	m_hours( _hours )
	,	m_minutes( _minutes )
{
} // RttyOperator::RttyOperator

/*---------------------------------------------------------------------------*/

RttyOperator::~RttyOperator()
{
} // RttyOperator::~RttyOperator

/*---------------------------------------------------------------------------*/

int
RttyOperator::getReport() const
{
	return m_report;

} // RttyOperator::getReport

/*---------------------------------------------------------------------------*/

int
RttyOperator::getSerialNumber() const
{
	return m_serialNumber;

} // RttyOperator::getSerialNumber

/*---------------------------------------------------------------------------*/

int
RttyOperator::getHours() const
{
	return m_hours;

} // RttyOperator::getHours

/*---------------------------------------------------------------------------*/

int
RttyOperator::getMinutes() const
{
	return m_minutes;

} // RttyOperator::getMinutes

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample
