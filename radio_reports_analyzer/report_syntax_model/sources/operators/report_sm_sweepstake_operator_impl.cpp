// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#include "radio_reports_analyzer/report_syntax_model/sources/ph/report_sm_ph.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/operators/report_sm_sweepstake_operator_impl.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

SweepstakeOperator::SweepstakeOperator(
		const ReportSyntaxModel::Line & _line
	,	const std::string & _callsign
	,	int _serialNumber
	,	SweepstakePrecedence::Enum _precedence
	,	int _digitCheck
	,	const std::string & _section
)
	:	BaseOperator(
				_line
			,	_callsign
		)
	,	m_serialNumber( _serialNumber )
	,	m_precedence( _precedence )
	,	m_digitCheck( _digitCheck )
	,	m_section( _section )
{
} // SweepstakeOperator::SweepstakeOperator

/*---------------------------------------------------------------------------*/

SweepstakeOperator::~SweepstakeOperator()
{
} // SweepstakeOperator::~SweepstakeOperator

/*---------------------------------------------------------------------------*/

int
SweepstakeOperator::getSerialNumber() const
{
	return m_serialNumber;

} // SweepstakeOperator::getSerialNumber

/*---------------------------------------------------------------------------*/

SweepstakePrecedence::Enum
SweepstakeOperator::getPrecedence() const
{
	return m_precedence;

} // SweepstakeOperator::getPrecedence

/*---------------------------------------------------------------------------*/

int
SweepstakeOperator::getDigitCheck() const
{
	return m_digitCheck;

} // SweepstakeOperator::getDigitCheck

/*---------------------------------------------------------------------------*/

const std::string &
SweepstakeOperator::getSection() const
{
	return m_section;

} // SweepstakeOperator::getSection

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample
