// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_OPERATOR_BASE_IMPLEMENTATION_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_OPERATOR_BASE_IMPLEMENTATION_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

template< typename _Operator >
class OperatorBase
	:	public _Operator
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	OperatorBase(
			const Line & _line
		,	const std::string & _callsign
	);

	/*virtual*/ ~OperatorBase();

/*---------------------------------------------------------------------------*/

	/*virtual*/ const Line & getLine() const;

	/*virtual*/ const std::string & getCallsign() const;

/*---------------------------------------------------------------------------*/

	/*virtual*/ void accept( OperatorVisitor & _visitor ) const;

/*---------------------------------------------------------------------------*/

protected:

/*---------------------------------------------------------------------------*/

	const Line & m_line;

	std::string m_callsign;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_OPERATOR_BASE_IMPLEMENTATION_HPP__
