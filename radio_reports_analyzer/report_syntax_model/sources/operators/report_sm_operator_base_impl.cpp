// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#include "radio_reports_analyzer/report_syntax_model/sources/ph/report_sm_ph.hpp"

#include "radio_reports_analyzer/report_syntax_model/api/report_sm_operator_visitor.hpp"
#include "radio_reports_analyzer/report_syntax_model/api/report_sm_ap_sprint_operator.hpp"
#include "radio_reports_analyzer/report_syntax_model/api/report_sm_darc_operator.hpp"
#include "radio_reports_analyzer/report_syntax_model/api/report_sm_iota_operator.hpp"
#include "radio_reports_analyzer/report_syntax_model/api/report_sm_na_sprint_operator.hpp"
#include "radio_reports_analyzer/report_syntax_model/api/report_sm_rtty_operator.hpp"
#include "radio_reports_analyzer/report_syntax_model/api/report_sm_sweepstake_operator.hpp"
#include "radio_reports_analyzer/report_syntax_model/api/report_sm_vhf_operator.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/operators/report_sm_operator_base_impl.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

template< typename _Operator >
OperatorBase< _Operator >::OperatorBase(
		const Line & _line
	,	const std::string & _callsign
)
	:	m_line( _line )
	,	m_callsign( _callsign )
{
} // OperatorBase< _Operator >::OperatorBase

/*---------------------------------------------------------------------------*/

template< typename _Operator >
OperatorBase< _Operator >::~OperatorBase()
{
} // OperatorBase< _Operator >::~OperatorBase

/*---------------------------------------------------------------------------*/

template< typename _Operator >
const Line &
OperatorBase< _Operator >::getLine() const
{
	return m_line;

} // OperatorBase< _Operator >::getLine

/*---------------------------------------------------------------------------*/

template< typename _Operator >
const std::string &
OperatorBase< _Operator >::getCallsign() const
{
	return m_callsign;

} // OperatorBase< _Operator >::getCallsign

/*---------------------------------------------------------------------------*/

template< typename _Operator >
void
OperatorBase< _Operator >::accept( OperatorVisitor & _visitor ) const
{
	_visitor.visit( * this );

} // OperatorBase< _Operator >::accept

/*---------------------------------------------------------------------------*/

template class OperatorBase< ApSprintOperator >;
template class OperatorBase< DarcOperator >;
template class OperatorBase< IotaOperator >;
template class OperatorBase< NaSprintOperator >;
template class OperatorBase< RttyOperator >;
template class OperatorBase< SweepstakeOperator >;
template class OperatorBase< VhfOperator >;

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample
