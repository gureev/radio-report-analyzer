// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#include "radio_reports_analyzer/report_syntax_model/sources/ph/report_sm_ph.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/operators/report_sm_vhf_operator_impl.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

VhfOperator::VhfOperator(
		const ReportSyntaxModel::Line & _line
	,	const std::string & _callsign
	,	const std::string & _grid
)
	:	BaseOperator(
				_line
			,	_callsign
		)
	,	m_grid( _grid )
{
} // VhfOperator::VhfOperator

/*---------------------------------------------------------------------------*/

VhfOperator::~VhfOperator()
{
} // VhfOperator::~VhfOperator

/*---------------------------------------------------------------------------*/

const std::string &
VhfOperator::getGrid() const
{
	return m_grid;

} // VhfOperator::getGrid

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample
