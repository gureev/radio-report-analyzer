// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#include "radio_reports_analyzer/report_syntax_model/sources/ph/report_sm_ph.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/operators/report_sm_iota_operator_impl.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

IotaOperator::IotaOperator(
		const ReportSyntaxModel::Line & _line
	,	const std::string & _callsign
	,	int _report
	,	int _serialNumber
	,	const std::string & _iotaDesignation
)
	:	BaseOperator(
				_line
			,	_callsign
		)
	,	m_report( _report )
	,	m_serialNumber( _serialNumber )
	,	m_iotaDesignation( _iotaDesignation )
{
} // IotaOperator::IotaOperator

/*---------------------------------------------------------------------------*/

IotaOperator::~IotaOperator()
{
} // IotaOperator::~IotaOperator

/*---------------------------------------------------------------------------*/

int
IotaOperator::getReport() const
{
	return m_report;

} // IotaOperator::getReport

/*---------------------------------------------------------------------------*/

int
IotaOperator::getSerialNumber() const
{
	return m_serialNumber;

} // IotaOperator::getSerialNumber

/*---------------------------------------------------------------------------*/

const std::string &
IotaOperator::getIotaDesignation() const
{
	return m_iotaDesignation;

} // IotaOperator::getIotaDesignation

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample
