// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_SWEEPSTAKE_OPERATOR_IMPLEMENTATION_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_SWEEPSTAKE_OPERATOR_IMPLEMENTATION_HPP__

/*---------------------------------------------------------------------------*/

#include "radio_reports_analyzer/report_syntax_model/api/report_sm_sweepstake_operator.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/operators/report_sm_operator_base_impl.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class SweepstakeOperator
	:	public OperatorBase< ReportSyntaxModel::SweepstakeOperator >
{

/*---------------------------------------------------------------------------*/

	typedef
		OperatorBase< ReportSyntaxModel::SweepstakeOperator >
		BaseOperator;

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	SweepstakeOperator(
			const ReportSyntaxModel::Line & _line
		,	const std::string & _callsign
		,	int _serialNumber
		,	SweepstakePrecedence::Enum _precedence
		,	int _digitCheck
		,	const std::string & _section
	);

	/*virtual*/ ~SweepstakeOperator();

/*---------------------------------------------------------------------------*/

	/*virtual*/ int getSerialNumber() const;

	/*virtual*/ SweepstakePrecedence::Enum getPrecedence() const;

	/*virtual*/ int getDigitCheck() const;

	/*virtual*/ const std::string & getSection() const;

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	int m_serialNumber;

	SweepstakePrecedence::Enum m_precedence;

	int m_digitCheck;

	std::string m_section;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_SWEEPSTAKE_OPERATOR_IMPLEMENTATION_HPP__
