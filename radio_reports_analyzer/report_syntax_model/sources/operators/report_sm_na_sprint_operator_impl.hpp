// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_NA_SPRINT_OPERATOR_IMPLEMENTATION_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_NA_SPRINT_OPERATOR_IMPLEMENTATION_HPP__

/*---------------------------------------------------------------------------*/

#include "radio_reports_analyzer/report_syntax_model/api/report_sm_na_sprint_operator.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/operators/report_sm_operator_base_impl.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class NaSprintOperator
	:	public OperatorBase< ReportSyntaxModel::NaSprintOperator >
{

/*---------------------------------------------------------------------------*/

	typedef
		OperatorBase< ReportSyntaxModel::NaSprintOperator >
		BaseOperator;

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	NaSprintOperator(
			const ReportSyntaxModel::Line & _line
		,	const std::string & _callsign
		,	int _serialNumber
		,	const std::string & _name
		,	const std::string & _qthDesignation
	);

	/*virtual*/ ~NaSprintOperator();

/*---------------------------------------------------------------------------*/

	/*virtual*/ int getSerialNumber() const;

	/*virtual*/ const std::string & getName() const;

	/*virtual*/ const std::string & getQthDesignation() const;

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	int m_serialNumber;

	std::string m_name;

	std::string m_qthDesignation;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_NA_SPRINT_OPERATOR_IMPLEMENTATION_HPP__
