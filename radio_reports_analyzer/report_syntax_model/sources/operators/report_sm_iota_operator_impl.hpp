// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_IOTA_OPERATOR_IMPLEMENTATION_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_IOTA_OPERATOR_IMPLEMENTATION_HPP__

/*---------------------------------------------------------------------------*/

#include "radio_reports_analyzer/report_syntax_model/api/report_sm_iota_operator.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/operators/report_sm_operator_base_impl.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class IotaOperator
	:	public OperatorBase< ReportSyntaxModel::IotaOperator >
{

/*---------------------------------------------------------------------------*/

	typedef
		OperatorBase< ReportSyntaxModel::IotaOperator >
		BaseOperator;

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	IotaOperator(
			const ReportSyntaxModel::Line & _line
		,	const std::string & _callsign
		,	int _report
		,	int _serialNumber
		,	const std::string & _iotaDesignation
	);

	/*virtual*/ ~IotaOperator();

/*---------------------------------------------------------------------------*/

	/*virtual*/ int getReport() const;

	/*virtual*/ int getSerialNumber() const;

	/*virtual*/ const std::string & getIotaDesignation() const;

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	int m_report;

	int m_serialNumber;

	std::string m_iotaDesignation;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_IOTA_OPERATOR_IMPLEMENTATION_HPP__
