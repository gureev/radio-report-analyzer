// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#include "radio_reports_analyzer/report_syntax_model/sources/ph/report_sm_ph.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/operators/report_sm_na_sprint_operator_impl.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

NaSprintOperator::NaSprintOperator(
		const ReportSyntaxModel::Line & _line
	,	const std::string & _callsign
	,	int _serialNumber
	,	const std::string & _name
	,	const std::string & _qthDesignation
)
	:	BaseOperator(
				_line
			,	_callsign
		)
	,	m_serialNumber( _serialNumber )
	,	m_name( _name )
	,	m_qthDesignation( _qthDesignation )
{
} // NaSprintOperator::NaSprintOperator

/*---------------------------------------------------------------------------*/

NaSprintOperator::~NaSprintOperator()
{
} // NaSprintOperator::~NaSprintOperator

/*---------------------------------------------------------------------------*/

int
NaSprintOperator::getSerialNumber() const
{
	return m_serialNumber;

} // NaSprintOperator::getSerialNumber

/*---------------------------------------------------------------------------*/

const std::string &
NaSprintOperator::getName() const
{
	return m_name;

} // NaSprintOperator::getName

/*---------------------------------------------------------------------------*/

const std::string &
NaSprintOperator::getQthDesignation() const
{
	return m_qthDesignation;

} // NaSprintOperator::getQthDesignation

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample
