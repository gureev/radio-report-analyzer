// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_AP_SPRINT_OPERATOR_IMPLEMENTATION_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_AP_SPRINT_OPERATOR_IMPLEMENTATION_HPP__

/*---------------------------------------------------------------------------*/

#include "radio_reports_analyzer/report_syntax_model/api/report_sm_ap_sprint_operator.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/operators/report_sm_operator_base_impl.hpp"

#include "framework/tools/common_headers/containers/vector.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class ApSprintOperator
	:	public OperatorBase< ReportSyntaxModel::ApSprintOperator >
{

/*---------------------------------------------------------------------------*/

	typedef
		OperatorBase< ReportSyntaxModel::ApSprintOperator >
		BaseOperator;

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	ApSprintOperator(
			const ReportSyntaxModel::Line & _line
		,	const std::string & _callsign
		,	int _report
		,	const std::string & _exchange
	);

	/*virtual*/ ~ApSprintOperator();

/*---------------------------------------------------------------------------*/

	/*virtual*/ int getReport() const;

	/*virtual*/ const std::string & getExchange() const;

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	int m_report;

	std::string m_exchange;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_AP_SPRINT_OPERATOR_IMPLEMENTATION_HPP__
