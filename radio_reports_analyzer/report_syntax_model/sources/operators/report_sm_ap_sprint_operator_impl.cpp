// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#include "radio_reports_analyzer/report_syntax_model/sources/ph/report_sm_ph.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/operators/report_sm_ap_sprint_operator_impl.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

ApSprintOperator::ApSprintOperator(
		const ReportSyntaxModel::Line & _line
	,	const std::string & _callsign
	,	int _report
	,	const std::string & _exchange
)
	:	BaseOperator(
				_line
			,	_callsign
		)
	,	m_report( _report )
	,	m_exchange( _exchange )
{
} // ApSprintOperator::ApSprintOperator

/*---------------------------------------------------------------------------*/

ApSprintOperator::~ApSprintOperator()
{
} // ApSprintOperator::~ApSprintOperator

/*---------------------------------------------------------------------------*/

int
ApSprintOperator::getReport() const
{
	return m_report;

} // ApSprintOperator::getReport

/*---------------------------------------------------------------------------*/

const std::string &
ApSprintOperator::getExchange() const
{
	return m_exchange;

} // ApSprintOperator::getExchange

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample
