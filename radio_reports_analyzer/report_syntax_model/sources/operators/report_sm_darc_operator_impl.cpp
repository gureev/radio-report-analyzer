// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#include "radio_reports_analyzer/report_syntax_model/sources/ph/report_sm_ph.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/operators/report_sm_darc_operator_impl.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

DarcOperator::DarcOperator(
		const ReportSyntaxModel::Line & _line
	,	const std::string & _callsign
	,	int _report
	,	const std::string & _exchange
)
	:	BaseOperator(
				_line
			,	_callsign
		)
	,	m_report( _report )
	,	m_exchange( _exchange )
{
} // DarcOperator::DarcOperator

/*---------------------------------------------------------------------------*/

DarcOperator::~DarcOperator()
{
} // DarcOperator::~DarcOperator

/*---------------------------------------------------------------------------*/

int
DarcOperator::getReport() const
{
	return m_report;

} // DarcOperator::getReport

/*---------------------------------------------------------------------------*/

const std::string &
DarcOperator::getExchange() const
{
	return m_exchange;

} // DarcOperator::getExchange

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample
