// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_RTTY_OPERATOR_IMPLEMENTATION_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_RTTY_OPERATOR_IMPLEMENTATION_HPP__

/*---------------------------------------------------------------------------*/

#include "radio_reports_analyzer/report_syntax_model/api/report_sm_rtty_operator.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/operators/report_sm_operator_base_impl.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class RttyOperator
	:	public OperatorBase< ReportSyntaxModel::RttyOperator >
{

/*---------------------------------------------------------------------------*/

	typedef
		OperatorBase< ReportSyntaxModel::RttyOperator >
		BaseOperator;

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	RttyOperator(
			const ReportSyntaxModel::Line & _line
		,	const std::string & _callsign
		,	int _report
		,	int _serialNumber
		,	int _hours
		,	int _minutes
	);

	/*virtual*/ ~RttyOperator();

/*---------------------------------------------------------------------------*/

	/*virtual*/ int getReport() const;

	/*virtual*/ int getSerialNumber() const;

	/*virtual*/ int getHours() const;

	/*virtual*/ int getMinutes() const;

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	int m_report;

	int m_serialNumber;

	int m_hours;

	int m_minutes;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_RTTY_OPERATOR_IMPLEMENTATION_HPP__
