// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_IMPLEMENTATION_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_IMPLEMENTATION_HPP__

/*---------------------------------------------------------------------------*/

#include "radio_reports_analyzer/report_syntax_model/writable/report_sm_file.hpp"
#include "radio_reports_analyzer/report_syntax_model/writable/report_sm_line.hpp"

#include "framework/tools/common_headers/containers/vector.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class File
	:	public Writable::File
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	File( boost::shared_ptr< FileSystem::Path > _filePath );

	/*virtual*/ ~File();

/*---------------------------------------------------------------------------*/

	/*virtual*/ boost::shared_ptr< FileSystem::Path > getPath() const;

/*---------------------------------------------------------------------------*/

	/*virtual*/ boost::optional< const ReportSyntaxModel::FileHeader & > getHeader() const;

/*---------------------------------------------------------------------------*/

	/*virtual*/ int getLinesCount() const;

	/*virtual*/ const Line & getLine( int _index ) const;

/*---------------------------------------------------------------------------*/

	/*virtual*/ void setHeader( std::auto_ptr< Writable::FileHeader > _fileHeader );

/*---------------------------------------------------------------------------*/

	/*virtual*/ Writable::Line & pushLine(
			LineCodeKind::Enum _codeKind
		,	int _frequency
		,	const std::string & _mode
		,	std::auto_ptr< Time > _time
	);

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	boost::shared_ptr< FileSystem::Path > m_filePath;

	boost::scoped_ptr< Writable::FileHeader > m_header;

/*---------------------------------------------------------------------------*/

	typedef
		CommonHeaders::Containers::Vector<
			boost::shared_ptr< Writable::Line >
		>
		Lines;

	Lines m_lines;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_IMPLEMENTATION_HPP__
