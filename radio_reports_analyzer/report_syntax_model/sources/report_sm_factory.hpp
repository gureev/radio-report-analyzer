// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FACTORY_IMPLEMENTATION_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FACTORY_IMPLEMENTATION_HPP__

/*---------------------------------------------------------------------------*/

#include "radio_reports_analyzer/report_syntax_model/api/report_sm_time.hpp"
#include "radio_reports_analyzer/report_syntax_model/api/report_sm_line_code_kind.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {

/*---------------------------------------------------------------------------*/

namespace Writable
{
	class Line;
}

/*---------------------------------------------------------------------------*/

namespace Implementation {

/*---------------------------------------------------------------------------*/

class Factory
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	static Factory & getInstance();

/*---------------------------------------------------------------------------*/

	std::auto_ptr< Writable::Line > createLine(
			LineCodeKind::Enum _codeKind
		,	int _frequency
		,	const std::string & _mode
		,	std::auto_ptr< Time > _time
	);

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	static boost::scoped_ptr< Factory > m_factory;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FACTORY_IMPLEMENTATION_HPP__
