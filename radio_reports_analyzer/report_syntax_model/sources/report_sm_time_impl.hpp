// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_TIME_IMPLEMENTATION_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_TIME_IMPLEMENTATION_HPP__

/*---------------------------------------------------------------------------*/

#include "radio_reports_analyzer/report_syntax_model/api/report_sm_time.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class Time
	:	public ReportSyntaxModel::Time
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	Time(
			int _year
		,	int _month
		,	int _day
		,	int _hours
		,	int _minutes
	);

	/*virtual*/ ~Time();

/*---------------------------------------------------------------------------*/

	/*virtual*/ int getMinutes() const;

	/*virtual*/ int getHours() const;

/*---------------------------------------------------------------------------*/

	/*virtual*/ int getDay() const;

	/*virtual*/ int getMonth() const;

	/*virtual*/ int getYear() const;

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	int m_year;

	int m_month;

	int m_day;

	int m_hours;

	int m_minutes;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_TIME_IMPLEMENTATION_HPP__
