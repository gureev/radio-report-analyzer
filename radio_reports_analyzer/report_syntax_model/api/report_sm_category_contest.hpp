// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_CATEGORY_CONTEST_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_CATEGORY_CONTEST_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {

/*---------------------------------------------------------------------------*/

class CategoryContest
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	enum Enum
	{
			Undefined

		,	Custom

		,	ApSprint

		,	Arrl10
		,	Arrl160
		,	ArrlDxCw
		,	ArrlDxSsb
		,	ArrlSsCw
		,	ArrlSsSsb
		,	ArrlUhfAug
		,	ArrlVhfJan
		,	ArrlVhfJun
		,	ArrlVhfSep
		,	ArrlRtty

		,	BartgRtty

		,	Cq160Cw
		,	Cq160Ssb
		,	CqWPxCw
		,	CqWPxRtty
		,	CqWPxSsb
		,	CqVHf
		,	CqWwCw
		,	CqWwRtty
		,	CqWwSsb

		,	DarcWaedcCw
		,	DarcWaedcRtty
		,	DarcWaedcSsb

		,	FcgFqp

		,	IaruHf

		,	JidxCw
		,	JidxSsb

		,	NaSprintCw
		,	NaSprintSsb

		,	NcccCqp

		,	Neqp

		,	OceaniaDxCw
		,	OceaniaDxSsb

		,	Rdxc

		,	RsgbIota

		,	SacCw
		,	SacSsb

		,	StewPerry

		,	TaraRtty

	};

/*---------------------------------------------------------------------------*/

	static const char * toString( Enum _enum )
	{
		switch ( _enum )
		{
		case Custom:			return "";

		case ApSprint:			return "AP-SPRINT";

		case Arrl10:			return "ARRL-10";
		case Arrl160:			return "ARRL-160";
		case ArrlDxCw:			return "ARRL-DX-CW";
		case ArrlDxSsb:			return "ARRL-DX-SSB";
		case ArrlSsCw:			return "ARRL-SS-CW";
		case ArrlSsSsb:			return "ARRL-SS-SSB";
		case ArrlUhfAug:		return "ARRL-UHF-AUG";
		case ArrlVhfJan:		return "ARRL-VHF-JAN";
		case ArrlVhfJun:		return "ARRL-VHF-JUN";
		case ArrlVhfSep:		return "ARRL-VHF-SEP";
		case ArrlRtty:			return "ARRL-RTTY";

		case BartgRtty:			return "BARTG-RTTY";

		case Cq160Cw:			return "CQ-160-CW";
		case Cq160Ssb:			return "CQ-160-SSB";
		case CqWPxCw:			return "CQ-WPX-CW";
		case CqWPxRtty:			return "CQ-WPX-RTTY";
		case CqWPxSsb:			return "CQ-WPX-SSB";
		case CqVHf:				return "CQ-VHF";
		case CqWwCw:				return "CQ-WW-CW";
		case CqWwRtty:			return "CQ-WW-RTTY";
		case CqWwSsb:			return "CQ-WW-SSB";

		case DarcWaedcCw:		return "DARC-WAEDC-CW";
		case DarcWaedcRtty:		return "DARC-WAEDC-RTTY";
		case DarcWaedcSsb:		return "DARC-WAEDC-SSB";

		case FcgFqp:			return "FCG-FQP";

		case IaruHf:			return "IARU-HF";

		case JidxCw:			return "JIDX-CW";
		case JidxSsb:			return "JIDX-SSB";

		case NaSprintCw:		return "NA-SPRINT-CW";
		case NaSprintSsb:		return "NA-SPRINT-SSB";

		case NcccCqp:			return "NCCC-CQP";

		case Neqp:				return "NCCC-CQP";

		case OceaniaDxCw:		return "OCEANIA-DX-CW";
		case OceaniaDxSsb:		return "OCEANIA-DX-SSB";

		case Rdxc:				return "RDXC";

		case RsgbIota:			return "RSGB-IOTA";

		case SacCw:				return "SAC-CW";
		case SacSsb:			return "SAC-SSB";

		case StewPerry:			return "STEW-PERRY";

		case TaraRtty:			return "TARA-RTTY";
		}

		return 0;

	} // toString

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_CATEGORY_CONTEST_HPP__
