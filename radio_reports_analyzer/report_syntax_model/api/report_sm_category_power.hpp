// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_CATEGORY_POWER_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_CATEGORY_POWER_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {

/*---------------------------------------------------------------------------*/

class CategoryPower
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	enum Enum
	{
			Undefined

		,	High
		,	Low
		,	Qrp

	};

/*---------------------------------------------------------------------------*/

	static const char * toString( Enum _enum )
	{
		switch ( _enum )
		{
		case High:				return "HIGH";
		case Low:				return "LOW";
		case Qrp:				return "QRP";

		case Undefined:			return "";
		}

		return 0;

	} // toString

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_CATEGORY_POWER_HPP__
