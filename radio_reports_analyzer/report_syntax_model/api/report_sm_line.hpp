// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_LINE_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_LINE_HPP__

/*---------------------------------------------------------------------------*/

#include "radio_reports_analyzer/report_syntax_model/api/report_sm_line_code_kind.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {

/*---------------------------------------------------------------------------*/

class Operator;
class Time;

/*---------------------------------------------------------------------------*/

class Line
	:	public boost::noncopyable
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	virtual ~Line() {}

/*---------------------------------------------------------------------------*/

	virtual LineCodeKind::Enum getCodeKind() const = 0;

/*---------------------------------------------------------------------------*/

	virtual int getFrequency() const = 0;

	virtual const std::string & getMode() const = 0;

	virtual const Time & getTime() const = 0;

/*---------------------------------------------------------------------------*/

	virtual const Operator & getSent() const = 0;

	virtual const Operator & getReceived() const = 0;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_LINE_HPP__
