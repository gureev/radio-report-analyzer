// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_CATEGORY_MODE_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_CATEGORY_MODE_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {

/*---------------------------------------------------------------------------*/

class CategoryMode
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	enum Enum
	{
			Undefined

		,	Ssb
		,	Cw
		,	Rtty
		,	Mixed

	};

/*---------------------------------------------------------------------------*/

	static const char * toString( Enum _enum )
	{
		switch ( _enum )
		{
		case Ssb:				return "SSB";
		case Cw:				return "CW";
		case Rtty:				return "RTTY";
		case Mixed:				return "MIXED";

		case Undefined:			return "";
		}

		return 0;

	} // toString

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_CATEGORY_MODE_HPP__
