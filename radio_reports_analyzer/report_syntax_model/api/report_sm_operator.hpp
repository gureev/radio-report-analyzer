// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_OPERATOR_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_OPERATOR_HPP__

/*---------------------------------------------------------------------------*/

#include "radio_reports_analyzer/report_syntax_model/api/report_sm_operator_kind.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {

/*---------------------------------------------------------------------------*/

class Line;
class OperatorVisitor;

/*---------------------------------------------------------------------------*/

class Operator
	:	public boost::noncopyable
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	virtual ~Operator() {}

/*---------------------------------------------------------------------------*/

	virtual OperatorKind::Enum getKind() const = 0;

/*---------------------------------------------------------------------------*/

	virtual const Line & getLine() const = 0;

	virtual const std::string & getCallsign() const = 0;

/*---------------------------------------------------------------------------*/

	virtual void accept( OperatorVisitor & _visitor ) const = 0;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_OPERATOR_HPP__
