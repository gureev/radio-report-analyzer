// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HEADER_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HEADER_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample
{
	namespace CommonHeaders
	{
		class Version;
	}
}

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {

/*---------------------------------------------------------------------------*/

class FileHeaderCategory;
class FileHeaderAddress;
class FileHeaderSoapbox;
class FileHeaderOperators;
class FileHeaderOfftime;
class FileHeaderClubs;
class FileHeaderContest;

/*---------------------------------------------------------------------------*/

class FileHeader
	:	public boost::noncopyable
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	virtual ~FileHeader() {}

/*---------------------------------------------------------------------------*/

	// START-OF-LOG:
	virtual const CommonHeaders::Version & getLogVersion() const = 0;

	// CLAIMED-SCORE:
	virtual boost::optional< int > getClaimedScore() const = 0;

	// CALLSIGN:
	virtual boost::optional< const std::string & > getCallsign() const = 0;

	// CREATED-BY:
	virtual boost::optional< const std::string & > getCreatedBy() const = 0;

	// EMAIL:
	virtual boost::optional< const std::string & > getEmail() const = 0;

	// LOCATION:
	virtual boost::optional< const std::string & > getLocation() const = 0;

	// NAME:
	virtual boost::optional< const std::string & > getName() const = 0;

	// CATEGORY-ASSISTED:
	// CATEGORY-BAND:
	// CATEGORY-MODE:
	// CATEGORY-OPERATOR:
	// CATEGORY-POWER:
	// CATEGORY-STATION:
	// CATEGORY-TIME:
	// CATEGORY-TRANSMITTER:
	// CATEGORY-OVERLAY:
	virtual boost::optional< const FileHeaderCategory & > getCategory() const = 0;

	// ADDRESS:
	// ADDRESS-CITY:
	// ADDRESS-STATE-PROVINCE:
	// ADDRESS-POSTALCODE:
	// ADDRESS-COUNTRY:
	virtual boost::optional< const FileHeaderAddress & > getAddress() const = 0;

	// CLUB:
	virtual boost::optional< const FileHeaderClubs & > getClubs() const = 0;

	// CONTEST:
	virtual boost::optional< const FileHeaderContest & > getContest() const = 0;

	// SOAPBOX:
	virtual boost::optional< const FileHeaderSoapbox & > getSoapbox() const = 0;

	// OPERATORS:
	virtual boost::optional< const FileHeaderOperators & > getOperators() const = 0;

	// OFFTIME:
	virtual boost::optional< const FileHeaderOfftime & > getOfftime() const = 0;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HEADER_HPP__
