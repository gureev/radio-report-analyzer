// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_CATEGORY_OPERATOR_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_CATEGORY_OPERATOR_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {

/*---------------------------------------------------------------------------*/

class CategoryOperator
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	enum Enum
	{
			Undefined

		,	SingleOp
		,	MultiOp
		,	Checklog

	};

/*---------------------------------------------------------------------------*/

	static const char * toString( Enum _enum )
	{
		switch ( _enum )
		{
		case SingleOp:			return "SINGLE-OP";
		case MultiOp:			return "MULTI-OP";
		case Checklog:			return "CHECKLOG";

		case Undefined:			return "";
		}

		return 0;

	} // toString

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_CATEGORY_OPERATOR_HPP__
