// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_EXCEPTIONS_VISITOR_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_EXCEPTIONS_VISITOR_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Exceptions {

/*---------------------------------------------------------------------------*/

class CannotAnalyzeFile;
class FileAlreadyAnalyzed;
class InvalidLogVersion;

/*---------------------------------------------------------------------------*/

class ExceptionsVisitor
	:	public boost::noncopyable
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	virtual ~ExceptionsVisitor() {}

/*---------------------------------------------------------------------------*/

	virtual void visit( const CannotAnalyzeFile & _exception ) = 0;

	virtual void visit( const FileAlreadyAnalyzed & _exception ) = 0;

	virtual void visit( const InvalidLogVersion & _exception ) = 0;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Exceptions
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_EXCEPTIONS_VISITOR_HPP__
