// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_OPERATOR_VISITOR_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_OPERATOR_VISITOR_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {

/*---------------------------------------------------------------------------*/

class ApSprintOperator;
class DarcOperator;
class IotaOperator;
class NaSprintOperator;
class RttyOperator;
class SweepstakeOperator;
class VhfOperator;

/*---------------------------------------------------------------------------*/

class OperatorVisitor
	:	public boost::noncopyable
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	virtual ~OperatorVisitor() {}

/*---------------------------------------------------------------------------*/

	virtual void visit( const ApSprintOperator & _operator ) = 0;

	virtual void visit( const DarcOperator & _operator ) = 0;

	virtual void visit( const IotaOperator & _operator ) = 0;

	virtual void visit( const NaSprintOperator & _operator ) = 0;

	virtual void visit( const RttyOperator & _operator ) = 0;

	virtual void visit( const SweepstakeOperator & _operator ) = 0;

	virtual void visit( const VhfOperator & _operator ) = 0;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_OPERATOR_VISITOR_HPP__
