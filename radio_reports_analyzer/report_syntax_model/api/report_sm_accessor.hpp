// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_ACCESSOR_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_ACCESSOR_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/connector/api/cn_accessor.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample
{
	namespace FileSystem
	{
		class Path;
	}
}

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {

/*---------------------------------------------------------------------------*/

class File;

/*---------------------------------------------------------------------------*/

class Accessor
	:	public Connector::Accessor
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	virtual const File & getFile( boost::shared_ptr< FileSystem::Path > _path ) const = 0;

	virtual boost::optional< const File & > findFile( boost::shared_ptr< FileSystem::Path > _path ) const = 0;

/*---------------------------------------------------------------------------*/

	virtual const File & build( boost::shared_ptr< FileSystem::Path > _path ) = 0;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_ACCESSOR_HPP__
