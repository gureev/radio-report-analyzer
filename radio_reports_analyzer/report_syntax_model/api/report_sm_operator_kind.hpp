// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_OPERATOR_KIND_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_OPERATOR_KIND_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {

/*---------------------------------------------------------------------------*/

class OperatorKind
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	enum Enum
	{
			Empty

		,	Sweepstake
		,	Vhf
		,	ApSprint
		,	Darc
		,	NaSprint
		,	Iota
		,	Rtty

	};

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_OPERATOR_KIND_HPP__
