// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HEADER_ADDRESS_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HEADER_ADDRESS_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {

/*---------------------------------------------------------------------------*/

class FileHeaderAddress
	:	public boost::noncopyable
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	virtual ~FileHeaderAddress() {}

/*---------------------------------------------------------------------------*/

	// ADDRESS:
	virtual const std::string & getAddress( int _index ) const = 0;

	virtual int getAdressesCount() const = 0;

	// ADDRESS-CITY:
	virtual const std::string & getCity() const = 0;

	// ADDRESS-STATE-PROVINCE:
	virtual const std::string & getState() const = 0;

	// ADDRESS-POSTALCODE:
	virtual const std::string & getPostalCode() const = 0;

	// ADDRESS-COUNTRY:
	virtual const std::string & getCountry() const = 0;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HEADER_ADDRESS_HPP__
