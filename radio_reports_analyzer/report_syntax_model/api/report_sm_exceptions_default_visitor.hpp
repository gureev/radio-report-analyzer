// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_EXCEPTIONS_DEFAULT_VISITOR_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_EXCEPTIONS_DEFAULT_VISITOR_HPP__

/*---------------------------------------------------------------------------*/

#include "radio_reports_analyzer/report_syntax_model/api/report_sm_exceptions_visitor.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Exceptions {

/*---------------------------------------------------------------------------*/

class ExceptionsDefaultVisitor
	:	public ExceptionsVisitor
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	virtual void visit( const CannotAnalyzeFile & /*_exception*/ ) {}

	virtual void visit( const FileAlreadyAnalyzed & /*_exception*/ ) {}

	virtual void visit( const InvalidLogVersion & /*_exception*/ ) {}

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Exceptions
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_EXCEPTIONS_DEFAULT_VISITOR_HPP__
