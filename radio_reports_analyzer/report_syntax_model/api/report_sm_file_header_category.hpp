// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HEADER_CATEGORY_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HEADER_CATEGORY_HPP__

/*---------------------------------------------------------------------------*/

#include "radio_reports_analyzer/report_syntax_model/api/report_sm_category_assisted.hpp"
#include "radio_reports_analyzer/report_syntax_model/api/report_sm_category_band.hpp"
#include "radio_reports_analyzer/report_syntax_model/api/report_sm_category_mode.hpp"
#include "radio_reports_analyzer/report_syntax_model/api/report_sm_category_operator.hpp"
#include "radio_reports_analyzer/report_syntax_model/api/report_sm_category_power.hpp"
#include "radio_reports_analyzer/report_syntax_model/api/report_sm_category_station.hpp"
#include "radio_reports_analyzer/report_syntax_model/api/report_sm_category_time.hpp"
#include "radio_reports_analyzer/report_syntax_model/api/report_sm_category_transmitter.hpp"
#include "radio_reports_analyzer/report_syntax_model/api/report_sm_category_overlay.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {

/*---------------------------------------------------------------------------*/

class FileHeaderCategory
	:	public boost::noncopyable
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	virtual ~FileHeaderCategory() {}

/*---------------------------------------------------------------------------*/

	// CATEGORY-ASSISTED:
	virtual CategoryAssisted::Enum getAssisted() const = 0;

	// CATEGORY-BAND:
	virtual CategoryBand::Enum getBand() const = 0;

	// CATEGORY-MODE:
	virtual CategoryMode::Enum getMode() const = 0;

	// CATEGORY-OPERATOR:
	virtual CategoryOperator::Enum getOperator() const = 0;

	// CATEGORY-POWER:
	virtual CategoryPower::Enum getPower() const = 0;

	// CATEGORY-STATION:
	virtual CategoryStation::Enum getStation() const = 0;

	// CATEGORY-TIME:
	virtual CategoryTime::Enum getTime() const = 0;

	// CATEGORY-TRANSMITTER:
	virtual CategoryTransmitter::Enum getTransmitter() const = 0;

	// CATEGORY-OVERLAY:
	virtual CategoryOverlay::Enum getOverlay() const = 0;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HEADER_CATEGORY_HPP__
