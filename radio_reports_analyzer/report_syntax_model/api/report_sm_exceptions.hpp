// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_EXCEPTIONS_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_EXCEPTIONS_HPP__

/*---------------------------------------------------------------------------*/

#include "radio_reports_analyzer/report_syntax_model/api/report_sm_exceptions_visitor.hpp"

#include "framework/exceptions/api/ex_exceptions.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample
{
	namespace FileSystem
	{
		class Path;
	}
}

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Exceptions {

/*---------------------------------------------------------------------------*/

EX_DECLARE_BASE_EXCEPTION(
		BaseException
	,	ExceptionsVisitor
)

/*---------------------------------------------------------------------------*/

EX_DECLARE_1_ARGUMENT(
		BaseException
	,	CannotAnalyzeFile
	,	( boost::shared_ptr< FileSystem::Path > )( filePath )
);

/*---------------------------------------------------------------------------*/

EX_DECLARE_1_ARGUMENT(
		BaseException
	,	FileAlreadyAnalyzed
	,	( boost::shared_ptr< FileSystem::Path > )( filePath )
);

/*---------------------------------------------------------------------------*/

EX_DECLARE_1_ARGUMENT(
		BaseException
	,	InvalidLogVersion
	,	( boost::shared_ptr< FileSystem::Path > )( filePath )
);

/*---------------------------------------------------------------------------*/

} // namespace Exceptions
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_EXCEPTIONS_HPP__
