// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample
{
	namespace FileSystem
	{
		class Path;
	}
}

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {

/*---------------------------------------------------------------------------*/

class FileHeader;
class Line;

/*---------------------------------------------------------------------------*/

class File
	:	public boost::noncopyable
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	virtual ~File() {}

/*---------------------------------------------------------------------------*/

	virtual boost::shared_ptr< FileSystem::Path > getPath() const = 0;

/*---------------------------------------------------------------------------*/

	virtual boost::optional< const FileHeader & > getHeader() const = 0;

/*---------------------------------------------------------------------------*/

	virtual int getLinesCount() const = 0;

	virtual const Line & getLine( int _index ) const = 0;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_FILE_HPP__
