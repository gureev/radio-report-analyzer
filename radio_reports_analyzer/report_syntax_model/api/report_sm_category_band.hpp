// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_CATEGORY_BAND_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_CATEGORY_BAND_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {

/*---------------------------------------------------------------------------*/

class CategoryBand
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	enum Enum
	{
			Undefined

		,	All
		,	Light

		,	V160M
		,	V80M
		,	V40M
		,	V20M
		,	V15M
		,	V10M
		,	V6M
		,	V2M
		,	V222
		,	V432
		,	V902
		,	V1o2G
		,	V2o3G
		,	V3o4G
		,	V5o7G
		,	V10G
		,	V24G
		,	V47G
		,	V75G
		,	V119G
		,	V142G
		,	V241G

	};

/*---------------------------------------------------------------------------*/

	static const char * toString( Enum _enum )
	{
		switch ( _enum )
		{
		case All:				return "ALL";
		case Light:				return "Light";
		case V160M:				return "160M";
		case V80M:				return "80M";
		case V40M:				return "40M";
		case V20M:				return "20M";
		case V15M:				return "15M";
		case V10M:				return "10M";
		case V6M:				return "60M";
		case V2M:				return "20M";
		case V222:				return "222";
		case V432:				return "432";
		case V902:				return "902";
		case V1o2G:				return "1.2G";
		case V2o3G:				return "2.3G";
		case V3o4G:				return "3.4G";
		case V5o7G:				return "5.7G";
		case V10G:				return "10G";
		case V24G:				return "24G";
		case V47G:				return "47G";
		case V75G:				return "75G";
		case V119G:				return "119G";
		case V142G:				return "142G";
		case V241G:				return "241G";

		case Undefined:			return "";
		}

		return 0;

	} // toString

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_CATEGORY_BAND_HPP__
