// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#ifndef __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_TEST_FIXTURE_HPP__
#define __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_TEST_FIXTURE_HPP__

/*---------------------------------------------------------------------------*/

#include "radio_reports_analyzer/report_syntax_model/writable/report_sm_file.hpp"

#include "radio_reports_analyzer/report_syntax_model/plugin/report_sm_plugin_instance.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Tests {

/*---------------------------------------------------------------------------*/

class Fixture
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	Fixture();

/*---------------------------------------------------------------------------*/

	const ReportSyntaxModel::File & getFile() const;

/*---------------------------------------------------------------------------*/

	void run( const std::string & _testCode );

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	void prepare();

/*---------------------------------------------------------------------------*/

	Plugin::Instance m_plugin;

	boost::shared_ptr< FileSystem::Accessor > m_fileSystem;

	boost::scoped_ptr< Writable::File > m_file;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Tests
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RADIO_REPORTS_ANALYZER_REPORT_SYNTAX_MODEL_TEST_FIXTURE_HPP__
