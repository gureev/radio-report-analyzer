// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#include "radio_reports_analyzer/report_syntax_model/sources/ph/report_sm_ph.hpp"

#include "radio_reports_analyzer/report_syntax_model/writable/report_sm_file.hpp"
#include "radio_reports_analyzer/report_syntax_model/writable/report_sm_file_header.hpp"

#include "radio_reports_analyzer/report_syntax_model/tests/report_sm_test_fixture.hpp"

#include "framework/tools/common_headers/version.hpp"

#include <boost/test/unit_test.hpp>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Tests {

/*---------------------------------------------------------------------------*/

/*

	TEST PLAN:
		1) Header
			1.1) START-OF-LOG

*/

/*---------------------------------------------------------------------------*/

BOOST_FIXTURE_TEST_SUITE( Suite_ReportSyntaxModel, Fixture )

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( ReportSyntaxModel_1_1_Header_StartOfLog )
{
	/*---------- Setup ----------*/

	const std::string testCode =
		"START-OF-LOG"
	;

	/*------- Test Action -------*/

	run( testCode );

	/*---------- Check ----------*/

	auto header = getFile().getHeader();

	BOOST_REQUIRE( header );
	//BOOST_REQUIRE( header->getLogVersion()

} // ReportSyntaxModel_1_1_Header_StartOfLog

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_SUITE_END() // Suite_Resources

/*---------------------------------------------------------------------------*/

} // namespace Tests
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample
