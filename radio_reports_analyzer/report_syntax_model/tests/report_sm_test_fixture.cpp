// (C) 2013 Ample Radio Reports Analyzer, Report Syntax Model

#include "radio_reports_analyzer/report_syntax_model/sources/ph/report_sm_ph.hpp"

#include "radio_reports_analyzer/report_syntax_model/api/report_sm_exceptions.hpp"

#include "radio_reports_analyzer/report_syntax_model/sources/report_sm_file_impl.hpp"
#include "radio_reports_analyzer/report_syntax_model/sources/analyzer/report_sm_analyzer.hpp"

#include "radio_reports_analyzer/report_syntax_model/plugin/report_sm_plugin_instance.hpp"

#include "radio_reports_analyzer/report_syntax_model/tests/report_sm_test_fixture.hpp"

#include "framework/filesystem/api/fs_accessor.hpp"
#include "framework/filesystem/api/fs_path.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace RadioReportsAnalyzer {
namespace ReportSyntaxModel {
namespace Tests {

/*---------------------------------------------------------------------------*/

Fixture::Fixture()
{
	Plugin::Environment & environment = m_plugin;

	m_fileSystem = environment.getFileSystem();

} // Fixture::Fixture

/*---------------------------------------------------------------------------*/

const ReportSyntaxModel::File &
Fixture::getFile() const
{
	EX_DEBUG_ASSERT( m_file );
	return * m_file;

} // Fixture::getFile

/*---------------------------------------------------------------------------*/

void
Fixture::run( const std::string & _testCode )
{
	prepare();

	Implementation::Analyzer analyzer( * m_fileSystem );
	analyzer.run( * m_file );

} // Fixture::run

/*---------------------------------------------------------------------------*/

void
Fixture::prepare()
{
	auto filePath = m_fileSystem->createPath( "test.txt" );

	m_file.reset( new Implementation::File( filePath ) );

} // Fixture::prepare

/*---------------------------------------------------------------------------*/

} // namespace Tests
} // namespace ReportSyntaxModel
} // namespace RadioReportsAnalyzer
} // namespace Ample
